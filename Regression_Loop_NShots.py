# this python scripte performs a loop on the number of  
# single shots used in the supervised regression of 
# observables from stacks single-shot images


#########################################################
#########                 IMPORTS               #########
#########################################################
import sys
sys.path.insert(1, './source/')
print(open('images/Uniqorn_logo.txt', 'r').read()+'\n\n')
print('UNIQORN: The Universal Neural-network Interface for Quantum Observable Readout from N-body wavefunctions'+'\n\n')

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from Models import clear as clearmodels
#import other custom-made modules and classes
import Input as inp 
import DataGenerator
import DataLoading
import Runtime_check                                                                                                                                                             
from functions import SpaceString
Runtime_check.runtime_checks()
del Runtime_check

histories=[] # array to collect history objects for all trainings
NSPS=np.arange(10, 30 , 10, dtype=int) # range for {N}umber of {S}ingle shots {P}er {S}ample
scores=[] # array to collect model errors
count=0

for i in NSPS:
  # modify inputs
  inp.Learn='RHO2'
  inp.momLabels=False
  inp.momData=True
  DataString=SpaceString(inp.momData)
  LabelString=SpaceString(inp.momLabels)
  fname='Errors_'+inp.Learn+'_in_'+LabelString+'_from_SSS_in_'+DataString
  inp.NShotsPerSample=i   
  inp.NShots=4*i
  inp.Loss='mae_logcosh'

  # Instantiation of the data generators for batch processing
  # with the paths to the files used as data.
  # This construction is needed to load the data and labels on the fly,
  # to not overload the memory for very large data sets.
  if inp.batch_loading==True:
     training_generator, validation_generator, y_val = DataGenerator.InitializeDataGenerators()
  elif inp.batch_loading==False:
     training_generator, validation_generator, X_train, y_train, X_val, y_val = DataGenerator.InitializeDataGenerators()
  
  y_val=np.array(y_val)
  ##################################################################################################################

  # import modules (to update dependencies on modified parameters from input)  
  import ModelTrainingAndValidation
  import ModelEvaluation

  if inp.batch_loading==True:
      (model, 
      history, 
      test_predictions,y_val) = ModelTrainingAndValidation.main(training_generator, validation_generator, y_val)
      
  elif inp.batch_loading==False:
      (model, 
      history, 
      test_predictions,y_val) = ModelTrainingAndValidation.main(X_train, y_train, X_val, y_val)
                                                     
  # append test results to scores arrays
  best_val_loss_index=np.argmin(np.array(history.history['val_loss']))
  print('best validation loss at epoch '+ str(best_val_loss_index))
  print(str(NSPS[0:count])) 
  print("shape of NSPS:"+str(np.shape(NSPS[0:count])))
  score=np.reshape(model.evaluate(validation_generator, verbose=0),(1,-1))
  score=np.insert(score,0,NSPS[count],axis=1)
  print('score: ' + str(score)+' best losses'+ str(np.float(history.history['loss'][best_val_loss_index]))+ str(np.float(history.history['val_loss'][best_val_loss_index])))
  score=np.insert(score,score.size-1,np.float(np.array(history.history['loss'][best_val_loss_index])),axis=1)
  score=np.insert(score,score.size-1,np.float(np.array(history.history['val_loss'])[best_val_loss_index]),axis=1)
  print('completed score: ' + str(score))

  print("shape of score:"+str(np.shape(score)))
  scores.append(score)
  print("shape of scores:"+str(np.shape(scores)))
  
#  np.savetxt('Errors_'+inp.Learn+'.txt',np.reshape(scores,(-1,6)))
  np.savetxt(fname+'.txt',np.squeeze(scores,axis=1))

  # increment loop counter
  count=count+1
  clearmodels()
  
# reshape scores for plotting
scores=np.squeeze(scores,axis=1)

# create plot of mean standard error and mean absolute error
legend=[]
fig, ax = plt.subplots()
ax.plot(NSPS,scores[:,4])
legend.append('loss')
ax.plot(NSPS,scores[:,5])
legend.append('val_loss')
ax.legend(legend, loc='best')

ax.set(xlabel='Single Shots per sample', ylabel='Error')
ax.grid()

fig.savefig(fname+".png")
