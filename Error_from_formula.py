# this module computes the (two-body) density from a set of single shots
# and computes the error in comparison to the exact (two-body) density

#########################################################
#########                 IMPORTS               #########
#########################################################
import sys
sys.path.insert(1, './source/')
print(open('images/Uniqorn_logo.txt', 'r').read()+'\n\n')
print('UNIQORN: The Universal Neural-network Interface for Quantum Observable Readout from N-body wavefunctions'+'\n\n')

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

#import other custom-made modules and classes
import Input as inp 
from functions import *
import DataGenerator
import DataLoading

#########################################################


NSPS=np.arange(10, 101, 10, dtype=int) # range for {N}umber of {S}ingle shots {P}er {S}ample
scores=[] # array to collect model errors


for i in NSPS:
  # modify inputs
  inp.Learn='DENS'
  inp.momLabels=True
  inp.momData=True
  inp.NDatasets=400
  if (inp.momLabels!=inp.momData):
    print('error evaluation only possible if -- both -- data and labels are either in real or in momentum space!!!')
    break

  DataString=SpaceString(inp.momData)
  LabelString=SpaceString(inp.momLabels)
  fname='Errors_'+inp.Learn+'_in_'+LabelString+'_from_SSS_in_'+DataString+'_from_formula_'+str(inp.NDatasets)+'_datasets'
  inp.NShotsPerSample=i   
  inp.NShots=i
 

  # import modules (to update dependencies on modified parameters from input)  
  
  print("NShotsPerSample :::"+str(inp.NShotsPerSample))

  # Instantiation of the data generators for batch processing
  # with the paths to the files used as data.
  # This construction is needed to load the data and labels on the fly,
  # to not overload the memory for very large data sets.
  if inp.batch_loading==True:
    print('the error evaluation works only with batch_loading==false!!!')
  elif inp.batch_loading==False:
    training_generator, validation_generator, X_train, y_train, X_val, y_val = DataGenerator.InitializeDataGenerators()
  
  
  x_data=np.reshape(np.concatenate((np.array(X_train),np.array(X_val))),(inp.NDatasets,256,i))

  if inp.Learn=='RHO2':
    y_data=np.reshape(np.concatenate((np.array(y_train),np.array(y_val))),(inp.NDatasets,256*256))
  elif inp.Learn=='DENS':
    y_data=np.reshape(np.concatenate((np.array(y_train),np.array(y_val))),(inp.NDatasets,256))
  else:
    print('error evaluation only possible for density (inp.Learn=DENS) and two-body density (inp.Learn=RHO2)')
    break

  print("shape of x_data"+str(np.shape(x_data)))
  print("shape of y_data"+str(np.shape(y_data)))
  y_val=np.array(y_val)
  ##################################################################################################################



  thiserr=[]
  for j in range(inp.NDatasets):
    
    print("shape of Single shots"+str(np.shape(x_data[j,:,:])))
    if inp.Learn=='RHO2':
      rho2=[]#np.array(rho2,(inp.Npoints,inp.Npoints))
      rho2=Rho2FromSS(x_data[j,:,:],inp.NShotsPerSample,inp.Npoints)
      print("norm of rho2 from SS:::"+str(np.sum(rho2[:,:])))
      # the 83.0 is from the weight of the DVR used in the UNIQORN dataset
      print("norm of rho2 from WF:::"+str(np.sum(y_data[j,:])/83.0))
      print("shape of rho2"+str(np.shape(rho2)))
      print("shape of label"+str(np.shape(y_data[j,:])))
      local_err=y_data[j,:]/83.0-np.reshape(rho2,inp.Npoints*inp.Npoints)
    elif inp.Learn=='DENS':
      rho=[]
      
      print("shape of Single shots"+str(np.shape(x_data[j,:,:])))
      rho=RhoFromSS(x_data[j,:,:],inp.NShotsPerSample,inp.Npoints)
      print("norm of rho from SS:::"+str(np.sum(rho[:])))
      print("norm of rho from WF:::"+str(np.sum(y_data[j])/np.sqrt(83.0)))
  
      print("shape of rho"+str(np.shape(rho)))
      
      print("shape of label"+str(np.shape(y_data[j,:])))
      # the sqrt(83.0) is from the weight of the DVR used in the UNIQORN dataset
      local_err=y_data[j,:]/np.sqrt(83.0)-np.reshape(rho,inp.Npoints)
 
    print("shape of error"+str(np.shape(local_err)))
    
    thiserr.append(np.sum(np.sqrt(local_err**2)))
    print("for dataset " +str(j)+ " I found an error of " + str(thiserr[j]) + "with "+ str(inp.NShotsPerSample) + "shots per sample")

  scores.append([i,np.mean(thiserr),np.var(thiserr),np.std(thiserr)])

# reshape scores for plotting
scores=np.reshape(scores,(-1,4))
np.savetxt(fname+'.txt',scores)

# create plot of mean standard error and mean absolute error
legend=[]
fig, ax = plt.subplots()
ax.errorbar(scores[:,0],scores[:,1],yerr=scores[:,3],fmt='o',ecolor='g')
legend.append('MSE')
ax.legend(legend, loc='best')

ax.set(xlabel='Single Shots per sample', ylabel='Error')
ax.grid()

fig.savefig(fname+".png")
print("shape of scores:::" + str(np.shape(str(scores))))
print(str(scores))
