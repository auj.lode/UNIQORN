#> Synopsis: Class that collects input variables for all the machine learning tasks
"""
Class for the definition of all input variables.

"""

#####################################################################################
#########               TRAINING DATA & DATA SET STRUCTURE                  #########
#####################################################################################

Training_Data='SSS'   # type of data used to extract information from the system.
                      # options are:
                      #    'SSS':  single shot simulations
                      #    'FRAG': for fragmentation.
                      #    'NPAR': for number of particles.
                      #    'DENS': for density distribution (real or momentum space)
                      #    'POT': for the potential 
                      #    'PHASEHIST': for the histogram of phases
                      #    'CORR1': Glauber g(1) correlation function (real or momentum space)
                      #    'CORR2': Glauber g(2) correlation function (real or momentum space)
                      #    'RHO1' : one-body reduced density matrix (absolute value squared)
                      #    'RHO2' : diagonal of the two-body reduced density matrix 

DirName='/home/axel/4TB/uniqorn_data/rand_DW'     # The datasets and labels are expected in subdirectories with the following names:
                      # <DirName>_<I>, where <DirName> is the string provided here and <I> is an Integer
                      # that runs from 0 to NDatasets-1
#####################################################################################

############################################
### DATA SET STRUCTURE FOR SINGLE SHOTS: ###
############################################

NDatasets=400         # number of wavefunctions to process
Npoints=256           # number of grid points in each single shot
NShots=400            # number of single shot pictures per wavefunction
                      # WARNING: NShots has to be divisible by NShotsPerSample (below)
NShotsPerSample=100   # number of single shots to stack together for images fed to the NN
TrainFraction=0.75    # TestFraction will be 1 - TrainFraction
                      # number of single shots (NShots) has to be divisible by this number (NShotsPerSample)
NormalizeData=False   # Toggle to normalize the data in regression tasks
momData= False        # toggle for switching between real and momentum space data
Plot= False           # visualize the single shots?

AxisFlipping=False   # use flipped images to generate more data?
DataThreshold=False    # discard the data between datathresholdlow and datathresholdhigh (in the training set)
DataThresholdTest=False # discard the data between datathresholdlow and datathresholdhigh also in the test set
DataThresholdLow = 0.03
DataThresholdHigh = 0.3

MultiSSSOrder=True  # use multiple configurations of SSS, i.e., the single-shot images are listed 
                      # in different orders for one wavefunction. 
MultiSSS_NOrder=3     # for each wavefunction, MultiSSS_NConfig different orders are used
#####################################################################################


#####################################################################################
#########                       BATCHING & EPOCHS                           #########
#####################################################################################

batch_loading = False # decide whether the data is loaded all in batches (True) or all at once (False).
batch_size = 10        # number of wavefunctions to process at a time
epochs = 200          # number of training iterations on all single shots
Randomize = False     # randomize input data sets?
Shuffle = True        # shuffle data after each epoch?
Loss   = 'mse'    # which loss function to use for the training? 
                      # Current Opts: 'mse' for mean squared error,
                      #               'mae' for mean absolute error
                      #               'log_mse' for log-mean-squared error
                      #               'log_cosh' for log-cosh error
                      #               'mse_mae' for summing MAE and MSE
                      #               'mse_logmse' for summing MSE and logMSE
                      #               'logmse_logcosh' for summing log_mse and log-cosh error
                      #               'mse_logcosh' for summing MSE and log-cosh error
                      #               'mae_logcosh' for summing MAE and log-cosh error
                      #               'logmse_logcosh_mae' for summing logMSE, logCosh and MAE 
#####################################################################################


#####################################################################################
#########                    MACHINE LEARNING TASKS                         #########
#####################################################################################

Job_Type='SUPERV_REGR'       # The task at hand.
Learn='DENS'                # what to fit model for.
                             # options are:
                             #    'FRAG': for fragmentation.
                             #    'NPAR': for number of particles.
                             #    'DENS': for density distribution (real or momentum space)
                             #    'POT': for the potential 
                             #    'PHASEHIST': for the histogram of phases
                             #    'CORR1': Glauber g(1) correlation function (real or momentum space)
                             #    'CORR2': Glauber g(2) correlation function (real or momentum space)
                             #    'RHO1' : one-body reduced density matrix (absolute value squared)
                             #    'RHO2' : diagonal of the two-body reduced density matrix 
#NOT USED SO FAR:
Dimensionality=1             # the dimensionality of the problem (needed for regression tasks on density, correlations etc.)


NViz=20                      # Number of plots of comparisons between predicted and true labels
Visualization_Save=True      # Save the visualized example data?

#Things the program can do (separately):
TrainingFlag=True             # Train the neural network? (no implementation yet for loading already trained NN!)

Visualization_Accuracies=True     # Visualize the model accuracy evaluated with the testing set? (requires TrainingFlag=True)
Visualization_Losses=True         # Visualize the model losses evaluated with the testing set? (requires TrainingFlag=True)
Visualization_Errors=True         # Visualize the errors of the training?
Visualization_ErrorHistograms=True # Visualize the Errors at every point of the test set in a Histogram?
VisualizationFlag=True            # Visualize the results of the neural network training?

Model='custom'                   # Where to load the neural network from?
                                  # options are:
                                  #           'default': predefined neural network in the python code:
                                  #                      sequential NN with layers 128->drop->128->drop->80->drop->1
                                  #                      !!! only works for regression !!!
                                  #           'archive': select an already tested neural network from an archive of
                                  #                      available options.
                                  #                      Need to provided 'Model_Type' and 'Model_Structure' below.
                                  #           'custom':  input all the parameters of the neural network by hand
                                  #

Model_Name='Seq128Dropout'       # The type of neural network for the algorithm chosen from the ones in the archive.
                                 # Requires Model='archive'.
                                 # options are:
                                 #           'Seq128Dropout': sequential MLP with layers 128->drop->128->drop->80->drop->1
                                 #           'DensityRegressionMLP': sequential MLP for regression of the density
                                 #                                   with layers=[512,256,128,64,Npoints] and
                                 #                                   regularizations=[0.0,0.3,0.1,0.01]
                                 #           'DensityRegressionCNN': sequential CNN for regression of the density
                                 #                                   with layers=[128,128,64,64,Npoints].
                                 #                                   !!! REQUIRES ConvNet=True!!!

ConvNet=False                    # Toggle for using convolutional neural networks (default is multilevel perceptrons)

callbacks = ['reduce_lr','earlystopping']         # include some callbacks in the training 
                                  # 'reduce_lr' (== reduce learning rate when fit doesn't improve)
                                  # 'earlystopping' (== stop training and return the best weights when fit is not improving)

layers=[512,256,128,64]                      # for Model=='custom' : use this list to create a stack of NNs
regularizations=[0.0,0.3,0.1,0.01]             # for Model=='custom' : use this list to define l2-regularizations
filters=[256,256,16]                 # for Model=='custom' : use this list to define the number of filters 
kernelsizes=[(1,50),(8,1),(8,1)] # for Model=='custom' : use this list to define the kernes sizes 
pooling=['max','none','max']      # for Model=='custom' : use this list to insert different types of pooling layers
batchnormalization=[True,False,False,False,False,False,False,False] # For HyperParameterOptimization the batchnormalization list needs the same amount of elements as the maximal number of layers (default==8) 
OutputDropout=0.3                 # dropout regularization applied before output layer
#####################################################################################


#####################################################################################
#########                       TASK-SPECIFIC FLAGS                         #########
#####################################################################################

############################
### FRAGMENTATION TASKS: ###
############################

FragmentationLowerBound = 0.01    #lower bound for the selection of fragmentation where to train the network
FragmentationUpperBound = 0.5     #upper bound for the selection of fragmentation where to train the network
FragmentationThreshold = 0.1      #threshold for the definition of a fragmented state:
num_classes = 2                   # number of intervals to classify fragmentation into

##############################
### PARTICLE NUMBER TASKS: ###
##############################

#bounds for the selection of particle number where to train the network:
NPartLowerBound = 1
NPartUpperBound = 100

##############################
###    DENSITY TASKS:    ###
##############################

momLabels=False       # Toggle for switching between real and momentum space in
                      # the regression task, i.e., if Learn='DENS','CORR1','CORR2','RHO1','RHO2'

NormalizeLabels=False # Toggle to normalize the data in regression tasks if Learn='DENS','CORR1', or 'CORR2'

InterpolatePredictions=False # Toggle for learning an interpolation instead of the values on the grid
LearnInterpolationKnots=False # Toggle for including/excluding the knots of the interpolation as outputs of the ANN
InterpolNpoints=10          # how many points to use for the interpolation?
RegularizationWeight=0.01   # what regularization weight to use for the spline
InterpolOrder=5             # what order should the spline be?

##############################
###    PHASE TASKS:        ###
##############################

PhaseBins=20          # How many bins should the phase histogram have that we want to learn in our regression
#####################################################################################
