#> Synopsis: a working example for a machine learning task using UNIQORN on MCTDH-X data.

#######################################################################
#######      RUNTIME CHECKS, INCLUDE SOURCE DIRECTORY           #######
#######################################################################

import sys
sys.path.insert(1, './source/')
print(open('images/Uniqorn_logo.txt', 'r').read()+'\n\n')
print('UNIQORN: The Universal Neural-network Interface for Quantum Observable Readout from N-body wavefunctions'+'\n\n')

import Runtime_check
Runtime_check.runtime_checks()
del Runtime_check

#########################################################

#########################################################
#########                 IMPORTS               #########
#########################################################

import numpy as np
from tensorflow.keras.utils import Sequence
from importlib import reload

#import UNIQORN modules and classes
import DataLoading
import DataPreprocessing
import DataGenerator
import ModelTrainingAndValidation
import Visualization
import Input as inp

#########################################################


###########################################################################
#######      LOADING FILENAMES AND INSTANTIATING DATA GENERATORS    #######
###########################################################################
# Instantiation of the data generators for batch processing
# with the paths to the files used as data.
# This construction is needed to load the data and labels on the fly,
# to not overload the memory for very large data sets.

if inp.batch_loading==True:
    training_generator, validation_generator, y_val = DataGenerator.InitializeDataGenerators()
elif inp.batch_loading==False:
    training_generator, validation_generator, X_train, y_train, X_val, y_val = DataGenerator.InitializeDataGenerators()

y_val=np.array(y_val)

###########################################################################


##########################################################
########                   MAIN                 ##########
##########################################################
#train (or load already trained) model and validate it  

if inp.TrainingFlag==True:   
    ##########################################################
    # Tweak input parameters to optimize performance of custom NNs
    ##########################################################
    #inp.layers=[1024,512,64]
    #inp.regularizations=[0.4,0.2,0.6]
    # ....

    if inp.batch_loading==True:
        (model, 
        history, 
        test_predictions,y_val) = ModelTrainingAndValidation.main(training_generator, validation_generator, y_val)
        
    elif inp.batch_loading==False:
        (model, 
        history, 
        test_predictions,y_val) = ModelTrainingAndValidation.main(X_train, y_train, X_val, y_val)
                                                                                                                                                                                                                                                                  
elif inp.TrainingFlag==False:                                                                                                                                                                                                                                 
    print('No neural network training requested. Please load the already trained neural network then: ')                                                                                                                                                          
    print('STILL NEEDS TO BE IMPLEMENTED IN DataTraining.py / Models.py!')                                                                                                                                                                                        
                                                                                                                                                                                                                                                                  
    #train (or load already trained) data with DataTraining module:                                                                                                                                                                                               
    (model, 
    test_predictions, y_val) =  ModelTrainingAndValidation.main()

####################################################################

##########################################################
########             VISUALIZATION              ##########
##########################################################

reload(Visualization)
if inp.VisualizationFlag==True:                                                                                                                                                                                                                               
                                                                                                                                                                                                                                                                 
   print('Accessing Visualization module...')                                                                                                                                                                                                                
   Visualization.main(y_val, history, test_predictions, inp.NViz)                                                                                                                                                          
                                                                                                                                                                                                                                                                  
elif inp.VisualizationFlag==False:                                                                                                                                                                                                                            
   print('No visualization of the results requested!')                                                                                                                                                                                                      
   print('\n')                                                                                                                                                                                                                                              
   print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')                                                                                                                                                                                                                    
   print('Closing up now. Bye, Felicia.')                                                                                                                                                                                                                   
   print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')                                                                                                                                                                                                                    
   print('\n')                                                                                                                                                                                                                                              
   exit()                                                                                                                                                                                                                                                                  

######################################################################  
