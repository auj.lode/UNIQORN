--- V\+E\+R\+S\+I\+ON 0.\+4 B\+E\+TA ---\hypertarget{md_README_autotoc_md1}{}\doxysubsection{Introduction}\label{md_README_autotoc_md1}
This repository contains some python and bash scripts that implement machine learning tasks using the Keras library. The code performs regression and/or classification tasks on various observables from data obtained from M\+C\+T\+D\+H-\/X simulations.

Currently, only single-\/shot images as input data are supported, but in the future using correlation functions as input will be implemented.

The quantities that can be analyzed so far are fragmentation, particle number, density distributions, the potential, and correlation functions.

Click this to download the single-\/shot \href{https://drive.google.com/file/d/1Du8KRhsITezlMVWEBrLOnDIAfFZOPcEj/view?usp=sharing}{\texttt{ D\+A\+TA}}.

This data should be placed in the same folder as the code in this repository. The data is a set of 3000 random ground states (randomized double wells with a random barrier height and width, random interactions, and a random particle number in them)\hypertarget{md_README_autotoc_md2}{}\doxysubsection{Prerequisites}\label{md_README_autotoc_md2}
As prerequisites to run the python scripts in this repository you will need (at least)


\begin{DoxyItemize}
\item Jupyter (for executing the notebook)
\item Keras (for neural network definition and training)
\item Tensorflow (backend for Keras)
\item Tensorflow-\/\+Addons (For using interpolation loss)
\item Matplotlib (for visualizing the results)
\item Numpy (for numerical manipulations)
\end{DoxyItemize}\hypertarget{md_README_autotoc_md3}{}\doxysubsection{Structure of the repository}\label{md_README_autotoc_md3}
Please refer to the flowchart \char`\"{}\+Workflow.\+pdf\char`\"{} for a graphical depiction of the structure of the modularized code. The python modules related to machine learning tasks are stored in the \char`\"{}source\char`\"{} directory and the python modules and files related to the data generation with M\+C\+T\+D\+H-\/X are stored in the directory \char`\"{}\+M\+C\+T\+D\+H\+X-\/data\+\_\+generation\char`\"{}. Every calculation is initiated by the jupyter notebook M\+C\+T\+D\+H-\/\+M\+L.\+ipynb, which dynamically accesses other modules that have different tasks.

P\+Y\+T\+H\+ON M\+O\+D\+U\+L\+ES\+:
\begin{DoxyItemize}
\item \mbox{\hyperlink{Input_8py}{Input.\+py}}\+: This python class contains all the parameters of the algorithm, ranging from which observable to reconstruct, to how to batch the input data, to what to visualize or not, etc.
\item \mbox{\hyperlink{DataPreprocessing_8py}{Data\+Preprocessing.\+py}}\+: This module performs runtime checks for input data consistency, and extracts and manipulates the input data via the subroutine \mbox{\hyperlink{DataLoading_8py}{Data\+Loading.\+py}} depending on the given task (i.\+e. supervised or unsupervised regression/classification) and on the given observable to fit (e.\+g. fragmentation, or density, or...). It also splits the input data into training and test sets.
\item \mbox{\hyperlink{DataLoading_8py}{Data\+Loading.\+py}}\+: This module imports the input data (single shots or correlation functions) and chooses which observables to use as labels (classification) or true values (regression).
\item \mbox{\hyperlink{DataGenerator_8py}{Data\+Generator.\+py}}\+: This module implements a class to dynamically load the data on-\/demand, i.\+e., batch-\/by-\/batch while training and validating the model.
\item \mbox{\hyperlink{ModelTrainingAndValidation_8py}{Model\+Training\+And\+Validation.\+py}}\+: This module constructs or loads from a library the type of neural network selected in the input file, compiles it, trains it on the training set, and validates it with the test set.
\item \mbox{\hyperlink{Models_8py}{Models.\+py}}\+: This module is a collection of python functions that return various types of neural networks and other Keras model objects to be used in \mbox{\hyperlink{ModelTrainingAndValidation_8py}{Model\+Training\+And\+Validation.\+py}}. There are default models depending on the task to be performed, an archive of tested models, and customizable models.
\item Model\+Evaluation.\+py\+: This module executes the validation of an already trained model with the test set.
\item \mbox{\hyperlink{Visualization_8py}{Visualization.\+py}}\+: This module generates and saves as .png files all the results of model training and testing (e.\+g. accuracies as a function of the epochs), as well as visual comparisons between the test set and the results obtained with the neural networks.
\item \mbox{\hyperlink{functions_8py}{functions.\+py}}\+: This module contains various useful functions to perform sub-\/tasks.
\item generate\+\_\+random\+\_\+\+D\+W\+\_\+states.\+py\+: This python script generates random relaxations with M\+C\+T\+D\+H-\/X software to produce the single-\/shot input data.
\end{DoxyItemize}

B\+A\+SH S\+C\+R\+I\+P\+TS (mainly used to generate or import data)\+:
\begin{DoxyItemize}
\item check\+\_\+convergence.\+sh\+: called by generate\+\_\+random\+\_\+\+D\+W\+\_\+states.\+py . Checks if a random relaxation is converged and restarts it, if not.
\item run\+\_\+anal.\+sh\+: called by generate\+\_\+random\+\_\+\+D\+W\+\_\+states.\+py . If a random relaxation is converged, this script runs the M\+C\+T\+D\+H-\/X analysis program on it.
\item M\+L\+G.\+sh\+: can be called to parse all python files for a certain string.
\end{DoxyItemize}

Currently the code supports only supervised regression tasks. The tasks can be implemented via a multilevel perceptron (M\+LP) or a convolutional neural network (C\+NN). Some default and also some customizable models are stored in the file \mbox{\hyperlink{Models_8py}{Models.\+py}}. Note that certain quantities such as fragmentation can only be inferred only from multiple (and not a single) single-\/shot images. The \mbox{\hyperlink{DataPreprocessing_8py}{Data\+Preprocessing.\+py}} function therefore assembles the input data in stacks of multiple single-\/shot images.\hypertarget{md_README_autotoc_md4}{}\doxysubsection{Running the code for a single set of hyperparameters}\label{md_README_autotoc_md4}
A good start is the jupyter notebook M\+C\+T\+D\+H-\/\+M\+L.\+ipynb, which calls all the different modules that perform various tasks.

You can check it out by typing

jupyter notebook

in your shell. This should open a window in your web browser, from which you can navigate to the file M\+C\+T\+D\+H\+X-\/\+M\+L.\+ipynb and execute it line by line. The notebook goes through the workflow explained above, i.\+e.

data loading -\/$>$ data processing -\/$>$ model choice, training and validation -\/$>$ visualization.

The notebook will automatically call and run other modules such as \mbox{\hyperlink{DataPreprocessing_8py}{Data\+Preprocessing.\+py}}, \mbox{\hyperlink{ModelTrainingAndValidation_8py}{Model\+Training\+And\+Validation.\+py}} etc. These files should be modified only if you are a developer implementing new machine learning tasks. Moreover, M\+C\+T\+D\+H\+X-\/\+M\+L.\+ipynb is to be seen as a starting point that trains, evaluates and visualizes a model for a single set of model parameters.

To choose which machine learning task to perform (e.\+g. switching from a regression of the particle number from single shots to a classification of fragmented/non-\/fragmented states from correlation functions), you need to modify the input file (python class) \mbox{\hyperlink{Input_8py}{Input.\+py}}. This file contains all the different knobs and variables for the machine learning algorithms, including hyperparameters such as batch size or number of epochs. In here, you can also select which quantity to fit and how, and whether or not you want to load a pre-\/trained neural network or train it yourself, visualize the results or not etc. The input file and the role of each variable therein should be self-\/explanatory.

Note that the notebook produces results in line while being executed, but the corresponding figures are also saved in the various folders in the main folder \char`\"{}plots\char`\"{} for later retrieval. The paths to these files and the files themselves are named after the quantity being fitted. For example, a plot of the accuracy of the regression of the particle number from single shots in real space during 20 epochs will be saved in the folder \char`\"{}plots/\+N\+P\+A\+R/accuracies\char`\"{}, with the name \char`\"{}\+Accuracies-\/for-\/\+R\+E\+G\+R-\/of-\/\+N\+P\+A\+R-\/from-\/\+S\+S\+S-\/in-\/x-\/space-\/during-\/20-\/epochs.\+png\char`\"{}\hypertarget{md_README_autotoc_md5}{}\doxysubsection{Performing a hyperparameter optimization with H\+P\+Bandster}\label{md_README_autotoc_md5}
It is a tough task to optimally configure all the possible parameters of deep learning models. However, since hyperparameter optimization is an optimization task, it can be automated. One library that provides out-\/of-\/the-\/box hyperparameter optimization is the Hp\+Band\+Ster library. See \href{https://github.com/automl/HpBandSter}{\texttt{ this link}} for details about Hp\+Band\+Ster. Obviously, the Hp\+Band\+Ster library is a prerequisite for running the code.

Currently, we only provide an Hp\+Band\+Ster implementation for optimizing convolutional neural networks (Set Model=\textquotesingle{}custom\textquotesingle{} and Conv\+Net=True in \mbox{\hyperlink{Input_8py}{Input.\+py}}). You can run the hyperparameter optimization by executing


\begin{DoxyItemize}
\item python \mbox{\hyperlink{HyperParameterOpt_8py}{Hyper\+Parameter\+Opt.\+py}}
\end{DoxyItemize}

This will then perform an optimization which you can visualize by running


\begin{DoxyItemize}
\item python \mbox{\hyperlink{PlotInteractiveHyperParameterOpt_8py}{Plot\+Interactive\+Hyper\+Parameter\+Opt.\+py}}
\end{DoxyItemize}

This will open plot the results of the optimization run. By clicking on the point in the plot with the lowest loss, you can find out the optimal set of hyperparameters, i.\+e., the result of the optimization. 