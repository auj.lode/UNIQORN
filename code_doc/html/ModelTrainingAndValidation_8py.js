var ModelTrainingAndValidation_8py =
[
    [ "ApplyInterpolation", "ModelTrainingAndValidation_8py.html#a99460a110e45e1c097e0bfe5371289aa", null ],
    [ "Interpolation_MSE", "ModelTrainingAndValidation_8py.html#ad27c49ce16f621987a45605736ab0606", null ],
    [ "LogCosh_plus_LogMSE", "ModelTrainingAndValidation_8py.html#aa10d14c5a1a04e89d2a2223471730cef", null ],
    [ "LogCosh_plus_LogMSE_plus_MAE", "ModelTrainingAndValidation_8py.html#ab9e5a4f64add0e89a8ff25ed1a20269d", null ],
    [ "LogCosh_plus_MAE", "ModelTrainingAndValidation_8py.html#a3045311deffce99c6b5ed3bd03337189", null ],
    [ "LogCosh_plus_MSE", "ModelTrainingAndValidation_8py.html#ac0e10f6fb7809d5240dfaa2957ac2218", null ],
    [ "main", "ModelTrainingAndValidation_8py.html#a5579cf6c9e1ee008204d7da0deb7751d", null ],
    [ "make_variable", "ModelTrainingAndValidation_8py.html#a529ed0b566655157e5fb81ae5bc23b75", null ],
    [ "ModelEvaluation", "ModelTrainingAndValidation_8py.html#a05c6e6a4e81a24473fd1f4ff5d6f10b8", null ],
    [ "MSE_plus_LogMSE", "ModelTrainingAndValidation_8py.html#a1e7ef2eff040ba9808137584650ea8bf", null ],
    [ "MSE_plus_MAE", "ModelTrainingAndValidation_8py.html#a6f60fccdbb730a0dce44691860f82bce", null ],
    [ "plot_interpol", "ModelTrainingAndValidation_8py.html#aa64a3f5e2418adb84853e460315f5d70", null ],
    [ "shape", "ModelTrainingAndValidation_8py.html#a650e92b7a7a317d022f0fd8cb4c344b9", null ],
    [ "size_labels", "ModelTrainingAndValidation_8py.html#af1c474c2f8d6da685745bc68fcb08574", null ],
    [ "size_predictions", "ModelTrainingAndValidation_8py.html#a6671ccffe79245ded47041f59acbd7e3", null ],
    [ "test_predictions", "ModelTrainingAndValidation_8py.html#af8511834518461a75e1dc1ba7e4172db", null ],
    [ "threshold", "ModelTrainingAndValidation_8py.html#ae3306bbe89ffd9c2061568be935a626c", null ],
    [ "xgrid", "ModelTrainingAndValidation_8py.html#ad9ea75875975b529e1e0ec2e52ba0a66", null ],
    [ "y_val", "ModelTrainingAndValidation_8py.html#aba73943f993e1661ad84b837e15bbea1", null ],
    [ "ygrid", "ModelTrainingAndValidation_8py.html#a7ed1d060eac2d568d67732e8ff55105d", null ]
];