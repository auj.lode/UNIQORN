var RHO2__X__from__SS__X__error_8py =
[
    [ "ax", "RHO2__X__from__SS__X__error_8py.html#ab2c74e46f4c180e906069a4351693163", null ],
    [ "data", "RHO2__X__from__SS__X__error_8py.html#a9b816f65694c7baba4ec34c460ccd1fd", null ],
    [ "ecolor", "RHO2__X__from__SS__X__error_8py.html#ade934f2d50fe6571cb190546934877f8", null ],
    [ "fig", "RHO2__X__from__SS__X__error_8py.html#a053db895ffdeb7236da82662f8cad862", null ],
    [ "fmt", "RHO2__X__from__SS__X__error_8py.html#a88ac593e0f36eb34618891ee9e60c71c", null ],
    [ "labels", "RHO2__X__from__SS__X__error_8py.html#a161f48fbd6c59ba0fd3e169e12809040", null ],
    [ "legend", "RHO2__X__from__SS__X__error_8py.html#a3116afddf2e052a3dbe5d47456a534ee", null ],
    [ "loc", "RHO2__X__from__SS__X__error_8py.html#ae2dd4e2b4fbfb87c3096d14e260dba14", null ],
    [ "local_err", "RHO2__X__from__SS__X__error_8py.html#ad57114013dc5ec3546491bfd0c162765", null ],
    [ "NShots", "RHO2__X__from__SS__X__error_8py.html#a062b1b5b975d0099057b4ace588de504", null ],
    [ "NShotsPerSample", "RHO2__X__from__SS__X__error_8py.html#a281d9b4e1cb73d706c39c581e245abf5", null ],
    [ "NSPS", "RHO2__X__from__SS__X__error_8py.html#aa75452548efce0df937e4803f8693bb4", null ],
    [ "rho2", "RHO2__X__from__SS__X__error_8py.html#a31a848da6192b8400448eb73a1010e14", null ],
    [ "scores", "RHO2__X__from__SS__X__error_8py.html#adb454f24ea0ed8706392a95edf735d21", null ],
    [ "thiserr", "RHO2__X__from__SS__X__error_8py.html#abca46faec43d19c2a6ea7a7ea91d91ae", null ],
    [ "Training_Data", "RHO2__X__from__SS__X__error_8py.html#a49847c66fd95b8917a7744badf4407d8", null ],
    [ "x_data", "RHO2__X__from__SS__X__error_8py.html#abf5b0377bb78b33834bb2d7fd4f64670", null ],
    [ "xlabel", "RHO2__X__from__SS__X__error_8py.html#a955d57c67cf61702f7ee13ea0e3ea96b", null ],
    [ "y_data", "RHO2__X__from__SS__X__error_8py.html#ab078b42a1aafd74b5cc5431d9b305d64", null ],
    [ "yerr", "RHO2__X__from__SS__X__error_8py.html#a84e94bd30fba5683ddca350ab142dddd", null ],
    [ "ylabel", "RHO2__X__from__SS__X__error_8py.html#a17764f4a9b4d324564bf95017704b522", null ]
];