var searchData=
[
  ['savehistorytofile_218',['SaveHistoryToFile',['../namespaceVisualization.html#a0cbd305495c4edc4a0b18173f8d1877e',1,'Visualization']]],
  ['scatterplot_219',['scatterplot',['../namespacerel__phase__package.html#a55abe676e9383fa3519e42abc87ea861',1,'rel_phase_package']]],
  ['scipyversion_220',['scipyVERSION',['../namespaceRuntime__check.html#a22d74b292cd6fbd7d336b17db527569a',1,'Runtime_check']]],
  ['score_221',['score',['../namespaceRegression__Loop__NShots.html#a5172d63896483d398f0bdcb93573c215',1,'Regression_Loop_NShots']]],
  ['scores_222',['scores',['../namespaceError__from__formula.html#a24a4c55230665c37c91bed461c773346',1,'Error_from_formula.scores()'],['../namespaceRegression__Loop__NShots.html#a390b4fe8556108350552c5d6b7a20a7c',1,'Regression_Loop_NShots.scores()']]],
  ['shape_223',['shape',['../namespaceModelTrainingAndValidation.html#a650e92b7a7a317d022f0fd8cb4c344b9',1,'ModelTrainingAndValidation']]],
  ['sharex_224',['sharex',['../namespaceVisualization.html#a7cb6bc042158650731c166352973acab',1,'Visualization']]],
  ['sharey_225',['sharey',['../namespaceVisualization.html#a3e46e6746c3d51841fc89f79a8a3121f',1,'Visualization']]],
  ['shuffle_226',['shuffle',['../classDataGenerator_1_1DataGenerator.html#a7185c14d23023742be56ca5b7224f909',1,'DataGenerator.DataGenerator.shuffle()'],['../namespaceInput.html#a747d2ea56ec6c70bd56f5fd4d6dfa502',1,'Input.Shuffle()']]],
  ['shutdown_5fworkers_227',['shutdown_workers',['../namespaceHyperParameterOpt.html#ab0d8d68fa29b39b8d97ef34a47e4821b',1,'HyperParameterOpt']]],
  ['size_5flabels_228',['size_labels',['../namespaceModelTrainingAndValidation.html#af1c474c2f8d6da685745bc68fcb08574',1,'ModelTrainingAndValidation']]],
  ['size_5fpredictions_229',['size_predictions',['../namespaceModelTrainingAndValidation.html#a6671ccffe79245ded47041f59acbd7e3',1,'ModelTrainingAndValidation']]],
  ['sleep_5finterval_230',['sleep_interval',['../classHyperParameterOpt__Worker_1_1MyWorker.html#a916cc68d1d4ed527c58d8033915b05d7',1,'HyperParameterOpt_Worker::MyWorker']]],
  ['spacestring_231',['SpaceString',['../namespacefunctions.html#afea643718472b07f3fc3e34ffeaa31b2',1,'functions']]],
  ['str_232',['str',['../namespaceHyperParameterOpt.html#a2c723b4bf5a88d2182fc25870c8e6423',1,'HyperParameterOpt']]]
];
