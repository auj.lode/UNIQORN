var searchData=
[
  ['validation_5fgenerator_257',['validation_generator',['../namespaceError__from__formula.html#a5f4f40c024fac5f754d91748cae32910',1,'Error_from_formula.validation_generator()'],['../namespaceRegression__Loop__NShots.html#a0c630385bb7a0e2be530ad19d5f18b92',1,'Regression_Loop_NShots.validation_generator()'],['../namespaceUNIQORN.html#a039ee79768acaa68705d96cd482301c3',1,'UNIQORN.validation_generator()']]],
  ['visualization_258',['Visualization',['../namespaceVisualization.html',1,'']]],
  ['visualization_2epy_259',['Visualization.py',['../Visualization_8py.html',1,'']]],
  ['visualization_5faccuracies_260',['Visualization_Accuracies',['../namespaceInput.html#a31193b9c1af34736035d11abb41621d0',1,'Input']]],
  ['visualization_5ferrorhistograms_261',['Visualization_ErrorHistograms',['../namespaceInput.html#ae4bd76a10129a3345fd6f22132a25b30',1,'Input']]],
  ['visualization_5ferrors_262',['Visualization_Errors',['../namespaceInput.html#ad57a3063d1c9aeb20236506795ff2049',1,'Input']]],
  ['visualization_5flosses_263',['Visualization_Losses',['../namespaceInput.html#a4e1a595543ba4c011c568d85246b4c12',1,'Input']]],
  ['visualization_5fsave_264',['Visualization_Save',['../namespaceInput.html#a7d06e696513f32a4f34f9a19a1988d33',1,'Input']]],
  ['visualizationflag_265',['VisualizationFlag',['../namespaceInput.html#a5851c7eefd25e9a359b11af02f6b50f5',1,'Input']]],
  ['vizgrid_266',['VizGrid',['../namespaceDataLoading.html#ac14885f9df3443baae8f76a9db744ac7',1,'DataLoading.VizGrid()'],['../namespaceVisualization.html#a73c79fcc95b0f60baca2dc17a37af9b5',1,'Visualization.VizGrid()']]],
  ['vizgrid_2etxt_267',['VizGrid.txt',['../VizGrid_8txt.html',1,'']]]
];
