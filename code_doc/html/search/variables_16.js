var searchData=
[
  ['x_5fdata_559',['x_data',['../namespaceError__from__formula.html#a9ec455ef2fda09fb10d162e2634e4a62',1,'Error_from_formula']]],
  ['x_5ftrain_560',['X_train',['../namespaceError__from__formula.html#a37226b8fbd862527dd3bbfe23e894b6a',1,'Error_from_formula.X_train()'],['../namespaceRegression__Loop__NShots.html#a41c9d52b4fcfd5641b1d90e34af2010d',1,'Regression_Loop_NShots.X_train()'],['../namespaceUNIQORN.html#a9419b44f0ccc1502ce35c6f21a477ae2',1,'UNIQORN.X_train()']]],
  ['x_5fval_561',['X_val',['../namespaceError__from__formula.html#a9cd8d2f8bb7982fd02cd8ef45f3398fa',1,'Error_from_formula.X_val()'],['../namespaceRegression__Loop__NShots.html#a6b3af3a7ee3379cb21c8fe2f6380e8dd',1,'Regression_Loop_NShots.X_val()'],['../namespaceUNIQORN.html#a5b314efdbe9ff9000e5a47e27b7161bd',1,'UNIQORN.X_val()']]],
  ['xgrid_562',['xgrid',['../namespaceModelTrainingAndValidation.html#ad9ea75875975b529e1e0ec2e52ba0a66',1,'ModelTrainingAndValidation']]],
  ['xlabel_563',['xlabel',['../namespaceError__from__formula.html#abc329c7f9fb94d5e3ca55704f7b9db42',1,'Error_from_formula.xlabel()'],['../namespaceRegression__Loop__NShots.html#acf78cc899714ef7f486d4969dac1dc76',1,'Regression_Loop_NShots.xlabel()']]],
  ['xmax_564',['Xmax',['../namespaceDataLoading.html#a1e2a5afc1729ecfc57d79fa7da3be7b5',1,'DataLoading']]],
  ['xmin_565',['Xmin',['../namespaceDataLoading.html#a76b99ca85db251229b69c1e47f7aa66e',1,'DataLoading']]]
];
