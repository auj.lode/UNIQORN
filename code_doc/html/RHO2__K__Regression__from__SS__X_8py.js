var RHO2__K__Regression__from__SS__X_8py =
[
    [ "ax", "RHO2__K__Regression__from__SS__X_8py.html#a69a3539ecfc9858ab4f7b86c8c764f1d", null ],
    [ "fig", "RHO2__K__Regression__from__SS__X_8py.html#abb7313d37774271213e5294f2aed1444", null ],
    [ "histories", "RHO2__K__Regression__from__SS__X_8py.html#a6072ad2aa104e70d07faec4d952553a0", null ],
    [ "legend", "RHO2__K__Regression__from__SS__X_8py.html#a9f575359790af418f9e40c10085fbd4d", null ],
    [ "loc", "RHO2__K__Regression__from__SS__X_8py.html#a95e9e98c61f53d7250e24f79cd978b3e", null ],
    [ "NShots", "RHO2__K__Regression__from__SS__X_8py.html#a8555545518077e615d830c5526bda029", null ],
    [ "NShotsPerSample", "RHO2__K__Regression__from__SS__X_8py.html#a398eb6f35c4f215ccbbf38dfe16bb970", null ],
    [ "NSPS", "RHO2__K__Regression__from__SS__X_8py.html#acf1fe5cc448a6ad069a694f9ba432e9b", null ],
    [ "score", "RHO2__K__Regression__from__SS__X_8py.html#ae9ddb4ce55769aceb2c680276c3a9d86", null ],
    [ "scores", "RHO2__K__Regression__from__SS__X_8py.html#a0744fda084caa0130f5ce09165520f8f", null ],
    [ "test_predictions", "RHO2__K__Regression__from__SS__X_8py.html#af27fded9b8f4c95aa1d8ad40613ad8dd", null ],
    [ "training_generator", "RHO2__K__Regression__from__SS__X_8py.html#ab9d55c3962b372b97595ac909613eab8", null ],
    [ "validation_generator", "RHO2__K__Regression__from__SS__X_8py.html#a0acbe56b8b68af02b01b98fe2b712424", null ],
    [ "X_train", "RHO2__K__Regression__from__SS__X_8py.html#a3a7aa86bdb1a88cfff4557e2fb78966d", null ],
    [ "X_val", "RHO2__K__Regression__from__SS__X_8py.html#a0b667e518c125365192f9802316a7bef", null ],
    [ "xlabel", "RHO2__K__Regression__from__SS__X_8py.html#af808dd4002073ab0041ba3aa5f0ab7fd", null ],
    [ "y_train", "RHO2__K__Regression__from__SS__X_8py.html#a021916f0e5f12caddf5a816b305bedf8", null ],
    [ "y_val", "RHO2__K__Regression__from__SS__X_8py.html#a882d16b8fe3068c099cb2357cb78eecb", null ],
    [ "ylabel", "RHO2__K__Regression__from__SS__X_8py.html#acabd56351c57ad65750ed18aa2eb1131", null ]
];