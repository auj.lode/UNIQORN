var Runtime__check_8py =
[
    [ "runtime_checks", "Runtime__check_8py.html#a3e0d4aac87e7ff2cdcc16c694c17548f", null ],
    [ "IDs_tot", "Runtime__check_8py.html#ae01a818e6fdae4502a9858a14d0350d0", null ],
    [ "matplotlibVERSION", "Runtime__check_8py.html#a2219d4757a1451b5fcf25fc05ec91fe2", null ],
    [ "NTestDatasets", "Runtime__check_8py.html#a6cc5811129a8b4590154a61bbab8667f", null ],
    [ "NTrainDatasets", "Runtime__check_8py.html#a5aa172f97066c17be05d02ed40f49024", null ],
    [ "numpyVERSION", "Runtime__check_8py.html#ac24f4d4ee72b64db7f81bbfc686da91c", null ],
    [ "pythonVERSION", "Runtime__check_8py.html#a6d145fdc5d70b3af8b6ef8e1c53cd711", null ],
    [ "scipyVERSION", "Runtime__check_8py.html#a22d74b292cd6fbd7d336b17db527569a", null ],
    [ "tfVERSION", "Runtime__check_8py.html#a8fc57ab646214acbf02bfab8f6dc2994", null ]
];