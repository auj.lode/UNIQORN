var UNIQORN_8py =
[
    [ "test_predictions", "UNIQORN_8py.html#a4a70342dd222924f50389126ef425390", null ],
    [ "training_generator", "UNIQORN_8py.html#a838898c77b2c8e11d5b438fd99d075f9", null ],
    [ "validation_generator", "UNIQORN_8py.html#a039ee79768acaa68705d96cd482301c3", null ],
    [ "X_train", "UNIQORN_8py.html#a9419b44f0ccc1502ce35c6f21a477ae2", null ],
    [ "X_val", "UNIQORN_8py.html#a5b314efdbe9ff9000e5a47e27b7161bd", null ],
    [ "y_train", "UNIQORN_8py.html#ac584dce3a379a22828eb9fa605e4c8de", null ],
    [ "y_val", "UNIQORN_8py.html#afcde9d310a2ea6bd896f6ceb60b37fe1", null ]
];