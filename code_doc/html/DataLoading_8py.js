var DataLoading_8py =
[
    [ "data_ID_loader", "DataLoading_8py.html#af68ec15e12cfe670c6b8851f18b3d30c", null ],
    [ "DataThresholdSelection", "DataLoading_8py.html#a7abc224cc999267b7b760c0349fe2d8b", null ],
    [ "extractor", "DataLoading_8py.html#a17f2d4738b0aa5b95712c56df5e514e8", null ],
    [ "label_ID_loader", "DataLoading_8py.html#aa3137bfdbe24792bb99e4236e009afb3", null ],
    [ "_", "DataLoading_8py.html#a4446d3f686413a296a9b18eafe2d0861", null ],
    [ "content", "DataLoading_8py.html#abe5e168761962bbdfebc03c7f4dd92fe", null ],
    [ "file_x_boundaries", "DataLoading_8py.html#ab05cc02252c82629b1991e0151a17687", null ],
    [ "FRAG", "DataLoading_8py.html#afbcf150873982a191b3cf2d3583aa907", null ],
    [ "last_line", "DataLoading_8py.html#a35e5dbcd87d610aebaa8b4bf71a81610", null ],
    [ "lines", "DataLoading_8py.html#a06272f1c6129c5daf56c7340208fee7f", null ],
    [ "no_of_columns", "DataLoading_8py.html#ac6ea0175a9d512a9e14f23cc3133e633", null ],
    [ "NShots", "DataLoading_8py.html#af858ef7bad085e3a694949f6369ef6a8", null ],
    [ "Ntot", "DataLoading_8py.html#aa0b25818f70f8aae048da225c3e033b0", null ],
    [ "ThisData", "DataLoading_8py.html#addc86d88289c0821b49ee3dbd4ab1c9f", null ],
    [ "toskip", "DataLoading_8py.html#a1d5fa3867f50e7c46a06c299f1bb7db7", null ],
    [ "VizGrid", "DataLoading_8py.html#ac14885f9df3443baae8f76a9db744ac7", null ],
    [ "Xmax", "DataLoading_8py.html#a1e2a5afc1729ecfc57d79fa7da3be7b5", null ],
    [ "Xmin", "DataLoading_8py.html#a76b99ca85db251229b69c1e47f7aa66e", null ]
];