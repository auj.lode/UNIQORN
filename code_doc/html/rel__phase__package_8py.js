var rel__phase__package_8py =
[
    [ "circular_mean", "rel__phase__package_8py.html#a34e8a22c37de317d38f571b5c74b5168", null ],
    [ "circular_var", "rel__phase__package_8py.html#ae4352b306f7121ca83277211944d3735", null ],
    [ "extract_info", "rel__phase__package_8py.html#af9c49f1d700eda96603558150b259a43", null ],
    [ "fit_plot", "rel__phase__package_8py.html#ac0308a0754055143f4b0090bbe97114a", null ],
    [ "gauss", "rel__phase__package_8py.html#af7c7382d42589edf3b18251a041613bf", null ],
    [ "initial_fit", "rel__phase__package_8py.html#aea0ba4080fc9ce97be54a8ad8e1e97a6", null ],
    [ "plot_phase_histogram", "rel__phase__package_8py.html#abd1b230c5f6ce61841adc06094117552", null ],
    [ "Pmodel", "rel__phase__package_8py.html#a84c456c236449af1bda2d8448a120bbd", null ],
    [ "scatterplot", "rel__phase__package_8py.html#a55abe676e9383fa3519e42abc87ea861", null ]
];