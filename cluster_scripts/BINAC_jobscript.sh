#PBS -S/bin/bash
#PBS -l nodes=1:ppn=1:gpus=1
#PBS -l walltime=720:00:00
#PBS -l mem=64GB
#PBS -N ultracold.org
#PBS -k oe
#PBS -m ea
#PBS -M auj.lode@gmail.com
#PBS -q gpu

PYTHON=/beegfs/work/workspace/ws/fr_al1112-axel2-0/anaconda3/bin/python
RUNTIMEINSEC=2590000
AIREPO=/beegfs/work/workspace/ws/fr_al1112-axel2-0/learning-single-shots
SCRIPT=./HyperParameterOpt.py

echo "Starting up at $(date +%s) in $PBS_O_WORKDIR"

cd $PBS_O_WORKDIR
cp ./BINAC_jobscript.sh ../BINAC_jobscript_$PBS_JOBID

#source ~/.bashrc
echo "sourced bashrc and initialized at $(date +%s)"

echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "copying AI program from $AIREPO to $TMPDIR" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
find $AIREPO/ -type f -maxdepth 1 ! -name Input.py -exec cp -t $TMPDIR/ {} + >> $PBS_O_WORKDIR/out.$PBS_JOBID
cp -r $AIREPO/source $TMPDIR >> $PBS_O_WORKDIR/out.$PBS_JOBID
cp $PBS_O_WORKDIR/Input.py $TMPDIR >> $PBS_O_WORKDIR/out.$PBS_JOBID
cp $PBS_O_WORKDIR/$SCRIPT $TMPDIR >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "DONE..." >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID

echo "copied in at $(date +%s)"

cd $TMPDIR

echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "Extracting data" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
tar -xf rand_DW_3000.tgz
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "DONE..." >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID

echo "extracted data at $(date +%s)"

echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "running AI program from at $PWD" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
#timeout $RUNTIMEINSEC python ./RHO2_K_Regression_from_SS_X.py >> $PBS_O_WORKDIR/out.$PBS_JOBID
#timeout $RUNTIMEINSEC python ./RHO2_K_Regression_from_SS_X.py
#timeout $RUNTIMEINSEC $PYTHON ./RHO2_K_Regression_from_SS_X.py >> $PBS_O_WORKDIR/out.$PBS_JOBID
timeout $RUNTIMEINSEC $PYTHON $SCRIPT >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "DONE..." >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID

echo "copying out at $(date +%s)"

echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "copying results from $TMPDIR to $PBS_O_WORKDIR" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
cp -r $TMPDIR/plots $PBS_O_WORKDIR/ >> $PBS_O_WORKDIR/out.$PBS_JOBID
find $TMPDIR/ -type f -maxdepth 1 ! -name rand_DW_3000.tgz -exec cp -t $PBS_O_WORKDIR/ {} + >> $PBS_O_WORKDIR/out.$PBS_JOBID
cp -r $TMPDIR/HyperParOpt-MCTDH-X $PBS_O_WORKDIR/ >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "DONE..." >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "#####################################" >> $PBS_O_WORKDIR/out.$PBS_JOBID
echo "done at $(date +%s)"
