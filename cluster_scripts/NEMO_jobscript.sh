#!/bin/bash
#MSUB -S /bin/bash
#MSUB -V
#MSUB -l nodes=1:ppn=32:gpus=1
#MSUB -l feature=amd
#MSUB -l walltime=96:00:00
#MSUB -l pmem=2GB
#MSUB -N http://ultracold.org
#MSUB -m bea
#MSUB -M auj.lode@gmail.com

PYTHON="/work/ws/nemo/fr_al1112-AI_STUFF-0/anaconda3/bin/python"
RUNTIMEINSEC=300000
AIREPO=/work/ws/nemo/fr_al1112-AI_STUFF-0/learning-single-shots

echo "Starting up at $(date +%s)"
module load devel/cmake/3.9 compiler/gnu/8.2 mpi/openmpi/3.1-gnu-8.2

cd $MOAB_SUBMITDIR
cp ./NEMO_jobscript.sh ../NEMO_jobscript_$MOAB_JOBID

source ~/.bashrc
echo "sourced bashrc and initialized at $(date +%s)"

echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "copying AI program from $AIREPO to $TMPDIR" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
find $AIREPO/ -type f -maxdepth 1 -exec cp -t $TMPDIR/ {} + >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
cp -r $AIREPO/source $TMPDIR >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "DONE..." >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID

echo "copied in at $(date +%s)"

cd $TMPDIR

echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "Extracting data" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
tar -xf rand_DW_3000.tgz
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "DONE..." >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID

echo "extracted data at $(date +%s)"

echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "running AI program from at $PWD" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
#timeout $RUNTIMEINSEC python ./RHO2_K_Regression_from_SS_X.py >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
exec timeout $RUNTIMEINSEC $PYTHON ./RHO2_K_Regression_from_SS_X.py 
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "DONE..." >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID

echo "copying out at $(date +%s)"

echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "copying results from $TMPDIR to $MOAB_SUBMITDIR" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
cp -r $TMPDIR/plots $MOAB_SUBMITDIR/ >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
find $TMPDIR/ -type f -maxdepth 1 ! -name rand_DW_3000.tgz -exec cp -t $MOAB_SUBMITDIR/ {} + >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
cp -r $TMPDIR/HyperParOpt-MCTDH-X $MOAB_SUBMITDIR/ >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "DONE..." >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "#####################################" >> $MOAB_SUBMITDIR/out.$MOAB_JOBID
echo "done at $(date +%s)"
