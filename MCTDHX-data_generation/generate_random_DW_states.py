### this script generates a bunch of random relaxations with MCTDH-X
### for this purpose it replaces certain placeholders ('rand_npar', 
### 'rand_p2',...) in a template ('MCTDHX.tmp') by random numbers.
### For every random MCTDH-X input file, a computation is run in a 
### separate directory and continued until convergence.
### When the computation in a directory has converged, an analysis 
### is performed (configured via analysis.inp) which yields single-shots.

### Prerequisites:
### input template: MCTDHX.tmp 
### analysis file: analysis.inp
### MCTDH-X executables: MCTDHX_gcc and MCTDHX_analysis_gcc


import sys
sys.path.insert(1, '../source/')
from functions import *
import random
import os
import numpy as np


# how many samples to generate
NSamples=10
# prefix of directories (suffix is the random instance)
dir_prefix='rand_DW'

# delete old samples
RunThis('rm -rf '+dir_prefix+'*')

# loop through samples
for i in range(NSamples-1):     
   # directory name and creation
   dir_string=dir_prefix+'_'+str(i)
   mkdir_string='mkdir '+dir_string
   RunThis(mkdir_string)
  
   # copy inputs and binary files to directory
   copy_string='cp MCTDHX.tmp '+dir_string+'/MCTDHX.inp'
   RunThis(copy_string)
   RunThis('cp check_convergence.sh analysis.inp MCTDHX_analysis_gcc MCTDHX_gcc '+dir_string)

   # randomize parameters in the input
   replaceAll(dir_string+'/MCTDHX.inp','rand_npar',str(random.randint(10,100)))
   replaceAll(dir_string+'/MCTDHX.inp','rand_p2',str(25.0-random.random()*20.0))
   replaceAll(dir_string+'/MCTDHX.inp','rand_p3',str(random.random()*3.0))
   replaceAll(dir_string+'/MCTDHX.inp','rand_p4',str(random.random()*0.5))

   # output directory to screen
   RunThis('pwd')
   RunThis('pwd')
   RunThis('pwd')


   # check convergence
   CONV=0

   while CONV<1:
     RunThis('cd '+dir_string+' && ./MCTDHX_gcc')    
     RunThis('cd '+dir_string+' && ./check_convergence.sh $PWD 10')   
     CONV=np.loadtxt(dir_string+'/conv.txt')
     print('found CONV:::'+str(CONV))
     print('found CONV:::'+str(CONV))
     print('found CONV:::'+str(CONV))
     print('found CONV:::'+str(CONV))
     #  if not converged, restart
     if CONV==0:
        print('RESULT NOT CONVERGED, RESTARTING')
        replaceAll(dir_string+'/MCTDHX.inp','HAND','BINR')
     #  if converged, run analysis
     if CONV==1:
        print('RESULT CONVERGED, Running analysis')

        RunThis('cd '+dir_string+' && ./MCTDHX_analysis_gcc')


