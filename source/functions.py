# this file collects useful functions 

#imports
import random
import fileinput
import sys
import subprocess
import numpy as np

import numpy as np

#########################################################
def SpaceString(selector):
  """ routine for assembling filenames, returns 'X' for 'False' (bool) input and 'K' for 'True' (bool) input """
  if (selector==True):
    return 'K'
  else:
    return 'X'




def G2FromSS(SingleShots,NShots,Npoints):
  
  """ routine that computes the two-body correlation function from a set of single shots """
  av = SingleShots.sum(axis=1)/NShots 
  g2 = np.zeros((Npoints,Npoints))
  for i in range(Npoints):
    g2[i,i] = -av[i]
    for j in range(Npoints):
      for s in range(NShots):
        g2[i,j] = g2[i,j] + SingleShots[i,s]*SingleShots[j,s]

  for i in range(Npoints):
    for j in range(Npoints):
      if abs(av[i]*av[j]) < 10.**(-16) :
        g2[i,j] = 0
      else :   
        g2[i,j] = g2[i,j]/(av[i]*av[j])

  return g2

def Rho2FromSS(SingleShots,NShots,Npoints):
  """ routine that computes the two-body density from a set of single shots """
  av = SingleShots.sum(axis=1)
  rho2 = np.zeros((Npoints,Npoints),dtype=float)

  for i in range(Npoints):
    rho2[i,i]=-av[i]

  for s in range(NShots):
    rho2=np.add(rho2,np.outer(SingleShots[:,s],SingleShots[:,s]))

  rho2 = rho2/float(NShots)
  return rho2

def RhoFromSS(SingleShots,NShots,Npoints):
  """ routine that computes the two-body density from a set of single shots """
  rho = np.zeros((Npoints),dtype=float)
  rho = SingleShots.sum(axis=1)
    
  rho = rho/float(NShots)
  return rho



def replaceAll(file,searchExp,replaceExp):
  """ replace 'searchExp' by 'replaceExp' in file 'file' """
  for line in fileinput.input(file, inplace=1):
      if searchExp in line:
          line = line.replace(searchExp,replaceExp)
      sys.stdout.write(line)

def RunThis(command):
  """    run command 'command' in the shell, store its output in 'output.txt' 
  and wait for the command to finish
  """    
  outfile = open("output.txt", "w")

  process = subprocess.Popen(command, shell=True, stdout=outfile)
  process.wait()
  outfile.close()


def randomize(a, b):
  """ shuffle the columns in two list using the same random permutation for both"""
  c = list(zip(a, b))
  random.shuffle(c)
  a, b = zip(*c)

  return a, b

def normalizer(array):
  """ normalize an array """
  summ = 10**(-16)   # minimal value for norm
  if not list(array): # return if we're not dealing with a list
      return [0]
  for element in array: # compute l2 norm
      summ = summ + abs(element)**2
  array_f = [] # create array to return normalized input 
  for i in range(len(array)): # fill output with normalized input
      array_f.append(array[i]/summ**0.5)
  return array_f

def histogrammer(data,LowerBound=-np.pi,UpperBound=np.pi,Bins=100):
  """ create a histogram with a certain number of bins """
  bins = np.linspace(LowerBound, UpperBound, Bins+1) 
  digitized = np.digitize(data, bins)
  return np.histogram(data, bins)[0]
  
