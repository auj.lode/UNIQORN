# This file collects methods for the data generators that are used to interface machine learning models
# with the single-shot dataset -- read while training or read to memory as a whole.
import numpy as np
import keras
from tensorflow.keras.utils import Sequence
import DataLoading
import DataPreprocessing
import Input as inp
from functions import *

import importlib
if importlib.util.find_spec("tqdm") is None:  # if tqdm is not installed, progress bar will not be activated
    tqdm_avail = False
else:
    import tqdm
    tqdm_avail = True

del importlib
##################################################################################################################
#########     DATA GENERATOR INITIALIZATION     #########
#########################################################
def InitializeDataGenerators():
    """
       This routine instantiates the DataGGenerator as training_generator and validation_generator
    """
    IDs_training, IDs_test = DataLoading.data_ID_loader(data=inp.Training_Data)
    
    
    training_generator = DataGenerator(IDs_training,
                                       batch_size=inp.batch_size,
                                       shuffle=inp.Shuffle)
    
    validation_generator = DataGenerator(IDs_test,
                                         batch_size=inp.batch_size,
                                         shuffle=inp.Shuffle)
    
    #Extract data depending on type of loading:
    if inp.batch_loading==True: #extract data on the fly
        # Extract the labels from the validation data generator:
        # They will be needed later when we compare the predictions with
        # the true labels.
        y_val=[]
        number_of_batches=validation_generator.__len__()
        for i in range(number_of_batches):
            X, y = validation_generator.__getitem__(i)
            y_val.append(y)
        y_val=np.array(y_val)
        print('________________________________________________________________________')
        print('-------------')
        print('Data generators used to load data on-the-fly:')
        print('-------------')
        print("Validation labels shape: "+str(np.shape(y_val)))
        print('________________________________________________________________________')

        return training_generator, validation_generator, y_val 
    
    elif inp.batch_loading==False: #extract data to memory all at once
        
        print('________________________________________________________________________')
        print('-------------')
        print('Data generators used to load data into memory:')
        print('-------------')
        X_train,y_train=training_generator.__getitem__(0)
        number_of_batches_train=training_generator.__len__()
        if tqdm_avail:
            iterate_temp = tqdm.trange(number_of_batches_train, desc='Loading training set data:')
        else:
            iterate_temp = range(number_of_batches_train)
        for i in iterate_temp:
            if i==0:
                continue
            X, y = training_generator.__getitem__(i)
            X_train=np.concatenate((X_train,X),axis=0)
            y_train=np.concatenate((y_train,y),axis=0)

        
        X_val,y_val=validation_generator.__getitem__(0)
        number_of_batches_val=validation_generator.__len__()
        if tqdm_avail:
            iterate_temp = tqdm.trange(number_of_batches_val, desc='Loading test set data:')
        else:
            iterate_temp = range(number_of_batches_val)
        for i in iterate_temp:
            if i==0:
                continue
            X, y = validation_generator.__getitem__(i)
            X_val=np.concatenate((X_val,X),axis=0)
            y_val=np.concatenate((y_val,y),axis=0)
        

        print("Training dataset shape: "+str(np.shape(X_train)))
        print("Training labels shape: "+str(np.shape(y_train)))
        print("Validation dataset shape: "+str(np.shape(X_val)))
        print("Validation labels shape: "+str(np.shape(y_val)))
        print('________________________________________________________________________')
        return training_generator, validation_generator, X_train, y_train, X_val, y_val 
##################################################################################################################




class DataGenerator(Sequence):
    """ Generates data for Keras """
    
    def __init__(self, list_IDs, shuffle=True, batch_size=5, #dim=(32,32,32), n_channels=1, n_classes=10
                ):
        """Initialization"""
        #self.dim = dim
        self.batch_size = batch_size        
        self.list_IDs = list_IDs    #list of directories/paths where to find the raw data
        #self.n_channels = n_channels
        #self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()
    
    def __len__(self):
        """ Denotes the number of batches per epoch """
        return int(np.floor(len(self.list_IDs) / self.batch_size))

        
    def __getitem__(self, index):
        """ Generate one batch of data """
        # Generate ID indexes for the current batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        #print(indexes)
        # Extract sublist of data directories/paths for the current batch
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        #print('Files for this batch:', list_IDs_temp)
        #print('\n')
        
        # Generate data via subroutine:
        X, y = self.__data_generation(list_IDs_temp, indexes)
    
        return X, y
        
        
    def on_epoch_end(self):
        """Updates indexes after each epoch"""
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)
            
            
    def __data_generation(self, list_IDs_temp, indexes):
        'Generates data containing batch_size samples'
        # Initialization and input variables:
        X = []
        y = []
        data = inp.Training_Data
        labels = inp.Learn
    
        # Generate data from provided set of directories/paths:
        for i, DataNameComplete in enumerate(list_IDs_temp):
    
            #self-consistent checking that DataNameComplete is not a list:
            if isinstance(DataNameComplete,list):
                DataNameComplete=str(DataNameComplete[0])
                
            # extract independent variables using extractor subroutine from DataLoading.py
            ThisData=DataLoading.extractor(data,DataNameComplete,i)
            #print('ThisData:',np.shape(ThisData))
            
            # If flipping along x axis is activated, check the index of the data. 
            # If the data comes from the second half of the training data, 
            # its positions will be flipped.
            # The flipping can be performed for both position and momentum data and labels.
            if ((inp.AxisFlipping) and (indexes[i]>len(self.indexes)/2) and \
                (data=='SSS' or data=='DENS' or data=='POT' or \
                 data=='CORR1' or data=='CORR2' or data=='RHO1' or data=='RHO2')):
                
                ThisData = np.flip(ThisData,-1)          

        

            #normalize dataset, if specified:
            if inp.NormalizeData==True:
                ThisData = np.array(normalizer(ThisData))
            # Store sample
            X.append(ThisData)

            #load label filename based on current data directory/path, via subroutine in DataLoading.py:
            LabelNameComplete = DataLoading.label_ID_loader(DataNameComplete, labels=inp.Learn)
            #print('LabelNameComplete:', LabelNameComplete)
            #self-consistency check:
            if isinstance(LabelNameComplete,list):
                LabelNameComplete=str(LabelNameComplete[0])
            # extract label from the file using extractor subroutine in DataLoading.py:
            ThisLabel=DataLoading.extractor(labels,LabelNameComplete,i)
            
            # flipping along the axis also for the labels
            if ((inp.AxisFlipping) and (indexes[i]>len(self.indexes)/2) and \
                (labels=='SSS' or labels=='DENS' or labels=='POT' or \
                 labels=='CORR1' or labels=='CORR2' or labels=='RHO1' or labels=='RHO2')):
                
                ThisLabel = np.flip(ThisLabel,-1)          

                
            #normalize label, if specified:
            if inp.NormalizeLabels==True:
                ThisLabel = np.array(normalizer(ThisLabel))
            # Store sample
            # Store label:
            y.append(ThisLabel)

        #Now the data and the labels will be preprocessed to fit the geometry of the ANN:
        X_preprocessed, y_preprocessed = DataPreprocessing.main(X, y)
        #print('Shape preprocessed data:', np.shape(X_preprocessed))
        #print('Shape preprocessed labels:', np.shape(y_preprocessed))

        return X_preprocessed, y_preprocessed

    #for classification in different classes, use the following:
    #return X, keras.utils.to_categorical(y, num_classes=self.n_classes)
