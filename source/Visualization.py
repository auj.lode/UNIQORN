# This module collects the routines to visualize the performance of the trained ANNs on the test set.

#########################################################
#########                 IMPORTS               #########
#########################################################
#portability imports:
from __future__ import print_function

#predefined packages:
import numpy as np
import sys
import matplotlib.pyplot as plt # for plotting history etc
import matplotlib.image as mpimg
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from mpl_toolkits.axes_grid1 import make_axes_locatable

import random # for chosing random samples from the test set
import re # regex support to parse history.keys
import os #to create folders if they do not exist


#custom-made classes:
import Input as inp  # import input file

#some options
np.set_printoptions(threshold=sys.maxsize)

#some global quantities for this file:
NSamples=int(inp.NShots/inp.NShotsPerSample) # number of samples, i.e., stacks of single shots

##################################################################################################################



##################################################################################################################
def main(y_test, history, test_predictions, NViz):
    """
    Main visualization function: visualizes and saves plots of training history, predictions etc.
    
    """
    
    #check whether folders where to save plots have been created and, if not, creates them:
    folder_creation()

    ############################################################
    # Flags for putting "k-space" or "x-space" in the name of
    # the files for the single shots / densities/ correlations:
    ############################################################
    if inp.momData==True:
        ss_label = 'k-space'
    else:
        ss_label = 'x-space'

    if inp.momLabels==True:
        space_label = 'k-space'
    else:
        space_label = 'x-space'
    ############################################################

    #plot history of NN performance:
    plot_history(history, ss_label, space_label)
    
    #plot comparison of NN predictions vs. true values:
    plot_predictions(y_test, test_predictions, NViz, ss_label, space_label)
    
    return
##################################################################################################################



##################################################################################################################
def plot_history(history, ss_label, space_label):
    """
    Generates plots of errors, accuracies, and losses of the neural network during training/testing.
    
    Parameters
    ----------
    y_test: numpy array, variable size and shape depending on dataset
        The labels or true values of the data set.
    
    history: keras object
        Object containing the training/testing history of the neural network in terms
        of error, losses, etc.
        
    test_predictions: numpy array, variable size and shape depending on dataset
        The predictions on the test set obtained from the trained neural network.
        
    NVIZ: int
        Number of figures to visualize for the direct comparison of 1D/2D data
        (e.g. density or correlations).
        
    ss_label: str
        Label indicating whether the single shots were in real or k space.
    
    space_label: str
        Label indicating whether the density/correlations were in real or k space.

    
    Returns
    -------
    NONE, visualizes and saves plots of training errors, accuracies, and losses.
    
    References
    ----------

    See Also
    --------

    Notes
    -----

    Examples
    --------
    
    """

#    if (inp.Visualization_Losses==True or inp.Visualization_Accuracies==True) and (inp.DataTrainingFlag=False):
#        print('You cannot print training accuracies or losses without performing training!')
#        print('Please select DataTrainingFlag==True in Input.py!')
#        print('\n')
#        print('Accuracies and losses have NOT been plotted. Continuing anyway with visualization.')
    
    # ERRORS
 
    if (inp.Visualization_Save):
       SaveHistoryToFile(history,history.history.keys(),
                         'plots/{1}/errors/History-for-{0}-in-{1}-of-{2}-from-{3}-in-{4}-during-{5}-epochs.dat'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs))

        

    if inp.Visualization_Errors==True:
        print('Visualization script found the following keys in the history:'+str(history.history.keys()))
        #-------------------------
        # Plot training & validation accuracy values
        #-------------------------
        legend=[]
        AddKeysToPlot(plt,history,history.history.keys(),'mse',legend)
        AddKeysToPlot(plt,history,history.history.keys(),'mae',legend)
        plt.legend(legend, loc='best')
        plt.title('Model errors')
        plt.ylabel('Errors')
        plt.xlabel('Epoch')
        if inp.Learn=='POT' or inp.Learn=='DENS' or inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO1' or inp.Learn=='RHO2':
            plt.savefig('plots/{1}/errors/Errors-for-{0}-in-{1}-of-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs), dpi=400) #density plots have additionally a flag describing whether they are in k-space or not.
         
        else:
            plt.savefig('plots/{1}/errors/Errors-for-{0}-of-{1}-from-{2}-in-{3}-during-{4}-epochs.png'.format(inp.Job_Type,inp.Learn,inp.Training_Data,ss_label,inp.epochs), dpi=300)
        plt.close()
        
    
    # ACCURACIES
    if inp.Visualization_Accuracies==True:
        # Plot training & validation accuracy values
        AddKeysToPlot(plt,history,history.history.keys(),'val_m',legend) # matches/plots val_mse and val_mae
        plt.title('Model validation accuracy')
        plt.ylabel('Accuracies')
        plt.xlabel('Epoch')
        plt.legend(legend, loc='best')
        if inp.Learn=='POT' or inp.Learn=='DENS' or inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO1' or inp.Learn=='RHO2':
            plt.savefig('plots/{1}/accuracies/Accuracies-for-{0}-in-{1}-of-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs), dpi=400) #density plots have additionally a flag describing whether they are in k-space or not.
         
        else:
            plt.savefig('plots/{1}/accuracies/Accuracies-for-{0}-of-{1}-from-{2}-in-{3}-during-{4}-epochs.png'.format(inp.Job_Type,inp.Learn,inp.Training_Data,ss_label,inp.epochs), dpi=300)
        plt.close()
 
 
    # LOSSES
    if inp.Visualization_Losses==True:
        # Plot training & validation loss values
        legend=[]
        AddKeysToPlot(plt,history,history.history.keys(),'loss',legend) # matches/plots loss and val_loss
        plt.title('Model loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(legend, loc='best')
        if inp.Learn=='POT' or inp.Learn=='DENS' or inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO1' or inp.Learn=='RHO2':
            plt.savefig('plots/{1}/losses/Losses-for-{0}-in-{1}-of-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs), dpi=400) #density plots have additionally a flag describing whether they are in k-space or not.
        else:
            plt.savefig('plots/{1}/losses/Losses-for-{0}-of-{1}-from-{2}-in-{3}-during-{4}-epochs.png'.format(inp.Job_Type,inp.Learn,inp.Training_Data,ss_label,inp.epochs), dpi=300)
            
        #TODO: To be included in the future: dynamically save the figures also with the model used in their name:
        plt.close()
            
    else:
        print('You have requested NOT to plot the model errors, nor accuracies, nor losses. Feeling risky?!?')
    
        #TODO: EXPAND TO INCLUDE PLOTS ONLY OF TEST ACCURACY/LOSSES WITHOUT TRAINING.
        
    return
##################################################################################################################



##################################################################################################################
###   PLOT OF THE PREDICTIONS FOR SUPERVISED REGRESSION FROM SINGLE SHOTS ###
#############################################################################
def plot_predictions(y_test, test_predictions, NViz, ss_label, space_label):
    """
    Generates plots of the predictions of the neural network compared with the true values.
        
    Parameters
    ----------
    y_test: numpy array, variable size and shape depending on dataset
        The labels or true values of the data set.
        
    test_predictions: numpy array, variable size and shape depending on dataset
        The predictions on the test set obtained from the trained neural network.
        
    NVIZ: int
        Number of figures to visualize for the direct comparison of 1D/2D data
        (e.g. density or correlations).
        
    ss_label: str
        Label indicating whether the single shots were in real or k space.
    
    space_label: str
        Label indicating whether the density/correlations were in real or k space.

    
    Returns
    -------
    NONE, visualizes and saves plots of averaged and non-averaged predictions against true values.
    
    References
    ----------

    See Also
    --------

    Notes
    -----

    Examples
    --------

    
    """
   

    if (inp.Job_Type=='SUPERV_REGR') and (inp.Training_Data=='SSS'):
    
        
        #dictionary of labels etc for the plots via subroutine:
        dict, lower_bound, upper_bound = get_labels()
        if (inp.Visualization_ErrorHistograms==True):
          #averaging of the results via subroutine:
          #TODO: EXTEND TO DENSITY, CORRELATIONS:
#          if inp.Learn=='FRAG' or inp.Learn=='NPAR':
              #unique_values, res, points = result_averaging(y_test, test_predictions)
        
#          else: # inp.Learn=='CORR1' or inp.Learn=='CORR2':
#              #TO BE COMPLETED:
#              test_predictions=test_predictions
        
          #------------------------------
          # Plot all (non-averaged) data
          #------------------------------
          fig,ax = plt.subplots() #plt.figure(dpi=100, figsize=(4, 4))
          plt.title('Non-averaged results')
          X=np.squeeze(np.reshape(y_test,(-1,1)),axis=1)
          Y=np.squeeze(np.reshape(test_predictions,(-1,1)),axis=1)
          print('shape '+ str(X.shape)+str(Y.shape))
 
          ax.set(adjustable='box', aspect='equal')  
          plt.axis('square')
          plt.setp(ax, ylim=ax.get_xlim())
          plt.setp(ax, xlim=ax.get_xlim())
          plt.xlabel('Actual '+dict['Learn'])
          plt.ylabel('Predicted '+dict['Learn'])
          min,max=np.amin(Y),np.amax(Y)
          import matplotlib.colors as mcolors
          plt.hist2d(X, Y, 100, cmap=plt.get_cmap('Blues'), range=[[min,max],[min,max]], norm=mcolors.LogNorm(vmin=1, vmax=2*inp.Npoints))#mcolors.PowerNorm(0.05))

          line = np.linspace(min,max)
          plt.plot(line, line, color="r")

          if inp.Learn=='FRAG' or inp.Learn=='NPAR':
              plt.xlim(lower_bound,upper_bound)
              plt.ylim(lower_bound,upper_bound)
          # plt.show()
          if inp.Learn=='POT' or inp.Learn=='DENS' or inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO1' or inp.Learn=='RHO2':
              plt.savefig('plots/{1}/Non-Averaged-{0}-in-{1}-of-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs), dpi=400) #density plots have additionally a flag describing whether they are in k-space or not.
          else:
              plt.savefig('plots/{1}/Non-Averaged-{0}-of-{1}-from-{2}-in-{3}-during-{4}-epochs.png'.format(inp.Job_Type,inp.Learn,inp.Training_Data,ss_label,inp.epochs), bbox_inches='tight', dpi=400)
          plt.close()
              
          #-----------------------------------------
          # Plot non-averaged results (as histogram)
          #----------------------------------------
          #TODO: add flag for plotting histogram or not.
          error = np.reshape((test_predictions - y_test),-1) #linearize array of errors to make a histogram
          plt.hist(error)
          plt.xlabel('Prediction Error [MPG]')
          plt.ylabel('Count')
          if inp.Learn=='POT' or inp.Learn=='DENS' or inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO1' or inp.Learn=='RHO2':
              plt.savefig('plots/{1}/Non-Averaged-Error-Histogram-{0}-of-{1}-in-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs), dpi=400) #density plots have additionally a flag describing whether they are in k-space or not.
          else:
              plt.savefig('plots/{1}/Non-Averaged-Error-Histogram-{0}-of-{1}-from-{2}-in-{3}-during-{4}-epochs.png'.format(inp.Job_Type,inp.Learn,inp.Training_Data,ss_label,inp.epochs), dpi=400)
          plt.close()
        
        
          #-----------------------
          # Plot averaged data
          #-----------------------
#          if inp.Learn=='FRAG' or inp.Learn=='NPAR':
#              fig = plt.figure(dpi=100, figsize=(4, 4))
#              plt.title('Averaged results')
#              plt.scatter(unique_values, res, marker='o', edgecolors='k')
#              plt.xlabel('Actual '+dict['Learn'])
#              plt.ylabel('Predicted '+dict['Learn'])
#              plt.xlim(lower_bound,upper_bound)
#              plt.ylim(lower_bound,upper_bound)
#              # plt.show()
#              plt.savefig('plots/{1}/Averaged-{0}-of-{1}-from-{2}-in-{3}-during-{4}-epochs.png'.format(inp.Job_Type,inp.Learn,inp.Training_Data,ss_label,inp.epochs), bbox_inches='tight', dpi=400)
#              plt.close()
             
        #--------------------------------------------------------------------
        # Plot comparisons between density predictions and labels
        #--------------------------------------------------------------------
        if (inp.Learn=='POT' or inp.Learn=='DENS'):
            # generate a random set of indices for the visualization
            TestingFraction=len(y_test[:,0])
            indices=[]
            for k in range(TestingFraction):
              indices.append(k)
            random.shuffle(indices)            

            # get the grid from file
            VizGrid=np.reshape(np.loadtxt('VizGrid.txt', usecols=[0]),(-1,1))

            print('Visualizing '+str(NViz)+' tests as <Comparison_XX.png> ...')
            if inp.Learn=='POT':
               Ylabel='Potential'
            elif inp.Learn=='DENS':
               Ylabel='Density'
            for i in range(NViz):
              plt.figure(dpi=150)
              plt.plot(VizGrid,test_predictions[indices[i],:])
              plt.plot(VizGrid,y_test[indices[i],:])
              plt.plot(VizGrid,y_test[indices[i],:]-test_predictions[indices[i],:])
              plt.xlabel("Position")
              plt.ylabel(Ylabel)
              plt.legend(['Predicted', 'Computed', 'Difference'], loc='upper left')
              plt.savefig('plots/{1}/Comparison-{6}-of-{0}-of-{1}-in-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs,i),  dpi=150, bbox_inches='tight')
              plt.close()

              # when the visualized data shall be stored, do so:
              if (inp.Visualization_Save):
                 Grid=np.savetxt('plots/{1}/Comparison-{6}-of-{0}-of-{1}-in-{2}-from-{3}-in-{4}-during-{5}-epochs.dat'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs,i),
                                 np.c_[np.reshape(VizGrid,(-1,1)),test_predictions[indices[i],:],y_test[indices[i],:]])
  

        ########################
        ### PHASE HISTOGRAMS
        ########################
        if (inp.Learn=='PHASEHIST'):
        
            #----------------------------------------------
            # Plot regression results (as correlation plot)
            #----------------------------------------------
            a = plt.axes(aspect='equal')
            from scipy.stats import gaussian_kde
            Z=gaussian_kde(np.vstack([x,y]))(np.vstack([x,y]))
            idx = Z.argsort()
            y_test, test_predictions,Z=y_test[idx],test_predictions[idx],Z[idx]

            plt.scatter(y_test, test_predictions,c=Z,s=100,edgecolor='')
           
            plt.xlabel('True Values [MPG]')
            plt.ylabel('Predictions [MPG]')
            plt.savefig('plots/{1}/Non-Averaged-{0}-in-{1}-of-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs), dpi=400)
            plt.close()
            
            #---------------------------------------
            # Plot regression results (as histogram)
            #---------------------------------------
            error =np.reshape((test_predictions - y_test),-1) #linearize array of errors to make a histogram
            plt.hist(error)
            plt.xlabel("Prediction Error [MPG]")
            plt.ylabel("Count")
            plt.savefig('plots/{1}/Non-Averaged-Error-Histogram-{0}-of-{1}-in-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs), dpi=400)
            plt.close()
            
            #---------------------------------------------------------
            # Plot comparisons between density predictions and labels
            #---------------------------------------------------------
            # generate a random set of indices for the visualization
            TestingFraction=len(y_test[:,0])
            indices=[]
            for k in range(TestingFraction):
              indices.append(k)
            random.shuffle(indices)            

            print('Visualizing '+str(NViz)+' tests as <Comparison_XX.png> ...')
            Ylabel='Distribution'
            for i in range(NViz):
              plt.figure(dpi=150)
              plt.plot(np.linspace(-np.pi, np.pi, inp.PhaseBins),test_predictions[indices[i],:])
              plt.plot(np.linspace(-np.pi, np.pi, inp.PhaseBins),y_test[indices[i],:])
              plt.xlabel("Phase [rad]")
              plt.ylabel(Ylabel)
              plt.legend(['Predicted', 'Computed'], loc='upper left')
              plt.savefig('plots/{1}/Comparison-{6}-of-{0}-of-{1}-in-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs,i),  dpi=150, bbox_inches='tight')
              plt.close()

#####        ax.hist(phases, density=1, bins=n_bins)
 
        ########################
        ### Correlations
        ########################
        if (inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO1' or inp.Learn=='RHO2'):
            #---------------------------------------------------------
            # Plot comparisons between density predictions and labels
            #---------------------------------------------------------
            TestingFraction=len(y_test[:,0])
            print('Visualizing '+str(NViz)+' tests as <Comparison_XX.png> ...')

            # generage a random set of indices for the visualization
            indices=[]
            for k in range(TestingFraction):
              indices.append(k)

            random.shuffle(indices)            

            titles=[]
            if (inp.Learn=='CORR1'):
              if inp.momLabels==True:
                titles.append(str(r"computed $\vert g^{(1)}(K,K') \vert$"))
                titles.append(str(r"predicted $\vert g^{(1)}(K,K') \vert$"))
                titles.append(str(r"difference"))

              else:
                titles.append(str(r"computed $\vert g^{(1)}(X,X') \vert$"))
                titles.append(str(r"predicted $\vert g^{(1)}(X,X') \vert$"))
                titles.append(str(r"difference"))

            elif (inp.Learn=='CORR2'):
              if inp.momLabels==True:
                titles.append(str(r"computed $g^{(2)}(K,K')$"))
                titles.append(str(r"predicted $g^{(2)}(K,K')$"))
                titles.append(str(r"difference"))

              else:
                titles.append(str(r"computed $g^{(2)}(X,X')$"))
                titles.append(str(r"predicted $g^{(2)}(X,X')$"))
                titles.append(str(r"difference"))

            elif (inp.Learn=='RHO1'):
              if inp.momLabels==True:
                titles.append(str(r"computed $\vert \rho^{(1)}(K,K')\vert^2$"))
                titles.append(str(r"predicted $\vert \rho^{(1)}(K,K')\vert^2$"))
                titles.append(str(r"difference"))
              else:
                titles.append(str(r"computed $\vert \rho^{(1)}(X,X')\vert^2$"))
                titles.append(str(r"predicted $\vert \rho^{(1)}(X,X')\vert^2$"))
                titles.append(str(r"difference"))
                
            elif (inp.Learn=='RHO2'):
              if inp.momLabels==True:
                titles.append(str(r"computed $\rho^{(2)}(K,K')$"))
                titles.append(str(r"predicted $\rho^{(2)}(K,K')$"))
                titles.append(str(r"difference"))

              else:
                titles.append(str(r"computed $\rho^{(2)}(X,X')$"))
                titles.append(str(r"predicted $\rho^{(2)}(X,X')$"))
                titles.append(str(r"difference"))
            
            for i in range(NViz):
                corrplot=[]
                corrplot.append(y_test[indices[i],:])
                corrplot.append(test_predictions[indices[i],:])
                corrplot.append(y_test[indices[i],:] - test_predictions[indices[i],:])
                corrplot=np.reshape(corrplot,(3,inp.Npoints,inp.Npoints))
                
                # get the grid from file
                VizGrid=np.reshape(np.loadtxt('VizGrid.txt', usecols=[0,1]),(-1,2))

                print('SHAPE OF CORRPLOT'+str(np.shape(corrplot)))
                print('PLOTTING INDEX '+str(indices[i]))
                plt.clf()
                fig, axes = plt.subplots(ncols=3, sharex=True, sharey=True )
                plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=None)

                j=0
                for ax in axes:
                #  ax.xlabel("Position X")
                #  ax.ylabel("Position X'")
                # pick the desired colormap, sensible levels, and define a normalization
                # instance which takes data values and translates those into levels.
                    cmap = plt.get_cmap('Blues')
                    c_max = np.amax(corrplot)
                    c_min = np.amin(corrplot)
                    ax.set_title(titles[j])
                    ax.set(adjustable='box', aspect='equal')
                    im=ax.pcolormesh(VizGrid[0:inp.Npoints,0],VizGrid[0:inp.Npoints,0], corrplot[j] , cmap=cmap, vmax=c_max, vmin=c_min)
                    # create an axes on the right side of ax. The width of cax will be 5%
                    # of ax and the padding between cax and ax will be fixed at 0.05 inch.
                    divider = make_axes_locatable(ax)
                    cax = divider.append_axes("right", size="5%", pad=0.05)
                    plt.colorbar(im,cax=cax)
                    j=j+1

                    plt.savefig('plots/{1}/Comparison-{6}-of-{0}-of-{1}-in-{2}-from-{3}-in-{4}-during-{5}-epochs.png'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs,i),  dpi=150, bbox_inches='tight')

                    if (inp.Visualization_Save):
                       Grid=np.savetxt('plots/{1}/Comparison-{6}-of-{0}-of-{1}-in-{2}-from-{3}-in-{4}-during-{5}-epochs.dat'.format(inp.Job_Type,inp.Learn,space_label,inp.Training_Data,ss_label,inp.epochs,i),
                                       np.c_[np.reshape(VizGrid,(-1,2)),test_predictions[indices[i],:],y_test[indices[i],:]])

                plt.close()
     
    print('\n')
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print('All plots successfully generated. Closing up now. Bye, Felicia.')
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print('\n')
        
    return
##################################################################################################################







#########################
#########################
####                 ####
####   SUBROUTINES   ####
####                 ####
#########################
#########################

def SaveHistoryToFile(HISTORY,keys,HistoryFileName):
    """
    This function saves the training History to a given file.
    """
    HistData=[]
    HistData=np.array(HistData)
    count = 0
    header='Epoch    '

    # loop through all keys, assemble numpy array
    for key in keys:
        if (count==0) :
           HistData=np.arange(1,np.array(HISTORY.history[key]).shape[0]+1,1)
        HistData=np.vstack((HistData,np.array(HISTORY.history[key])))
        header=header+key+'    '
        count=count+1

    # save the numpy array to a text file
    np.savetxt(HistoryFileName,np.transpose(HistData),header=header) 

    return

##################################################################################################################
def AddKeysToPlot(PLOT,HISTORY,keys,pattern,legend):
    """
    This function add keys to the legend in a plot.
    """
    for key in keys:
        prog=re.compile(str(pattern))
        if prog.match(key):
            PLOT.plot(HISTORY.history[key])
            legend.append(key)
    return
##################################################################################################################

##################################################################################################################
def grid_display(list_of_images, list_of_titles=[], no_of_columns=2, figsize=(10,10)):
    """
    This function displays a list of images.
    
    """
    
    
    fig = plt.figure(figsize=figsize)
    column = 0
    for i in range(len(list_of_images)):
        column += 1
        #  check for end of column and create a new figure
        if column == no_of_columns+1:
            fig = plt.figure(figsize=figsize)
            column = 1
        fig.add_subplot(1, no_of_columns, column)
        plt.imshow(list_of_images[i])
        plt.axis('off')
        if len(list_of_titles) >= len(list_of_images):
            plt.title(list_of_titles[i])
            
    return
##################################################################################################################

##################################################################################################################
def result_averaging(y_test, test_predictions):
    """
    Averaging of the prediction results for the same labels.
    
    
    """
    
    #--------------------------------------------------
    #Single out only unique values to plot and compare them with the average of the predicted values :
    #First construct a sorted list of unique values appearing in the y set:
#    unique_values = list(np.sort(list(set(y_test))))   # Unique values (e.g. unique particle number values)
    unique_values = list(np.sort(y_test))   # Unique values (e.g. unique particle number values)
    how_many_unique_values = len(unique_values)
      
    #initialize empty arrays that will contain the unique values
    res = np.zeros(how_many_unique_values)
    points = np.zeros(how_many_unique_values)
      
    #collect the same unique values predicted by the neural network and average them out:
    for i, n in enumerate(y_test):
        j = unique_values.index(n)           #get the index corresponding to the unique value in the y set
        res[j] += test_predictions[i]        #get the corresponding predicted values and sum them all up
        points[j] += 1.                      #count how many points with the same unique values have been predicted
    for j in range(how_many_unique_values):
        res[j] /= points[j]                  #average the results
    
    return unique_values, res, points
    #---------------------------------------------------
##################################################################################################################

##################################################################################################################
def get_labels():
    """
    Prepare dictionary of labels, upper/lower bounds for data for the plots, etc.
    
    """
    
    dict = {}
    if (inp.Learn=='FRAG'):
        dict['Learn']='Fragmentation'
        lower_bound=inp.FragmentationLowerBound
        upper_bound=inp.FragmentationUpperBound
        return dict, lower_bound, upper_bound

    elif (inp.Learn=='NPAR'):
        dict['Learn']='Particle Number'
        lower_bound=inp.NPartLowerBound
        upper_bound=inp.NPartUpperBound
        return dict, lower_bound, upper_bound

    elif (inp.Learn=='DENS'):
        dict['Learn']='Density'
        lower_bound='BOGUS'
        upper_bound='BOGUS'
        return dict, lower_bound, upper_bound

    elif (inp.Learn=='POT'):
        dict['Learn']='Potential'
        lower_bound='BOGUS'
        upper_bound='BOGUS'
        return dict, lower_bound, upper_bound

    elif (inp.Learn=='PHASEHIST'):
        dict['Learn']='Phase Histogram'
        lower_bound='BOGUS'
        upper_bound='BOGUS'
        return dict, lower_bound, upper_bound

    elif (inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO2' or inp.Learn=='RHO1'):
        dict['Learn']='Correlation'
        lower_bound='BOGUS'
        upper_bound='BOGUS'
        return dict, lower_bound, upper_bound

        
##################################################################################################################



##################################################################################################################
def folder_creation():
    """
    Checks wheter folders containing figures for visualization have been created and if not, creates them.
    
    """
    if not os.path.exists('plots'):
        os.makedirs('plots')
        
    if not os.path.exists('plots/{0}'.format(inp.Learn)):
            os.makedirs('plots/{0}'.format(inp.Learn))
            
    if not os.path.exists('plots/{0}/errors'.format(inp.Learn)):
            os.makedirs('plots/{0}/errors'.format(inp.Learn))
            
    if not os.path.exists('plots/{0}/accuracies'.format(inp.Learn)):
            os.makedirs('plots/{0}/accuracies'.format(inp.Learn))
            
    if not os.path.exists('plots/{0}/losses'.format(inp.Learn)):
            os.makedirs('plots/{0}/losses'.format(inp.Learn))
                        
    #this syntax didnt work:
    #try:
    #    os.makedirs('plots/{0}'.format(inp.Learn))
    #except OSError as e:
    #    if e.errno != errno.EEXIST:
    #        raise
    return
##################################################################################################################
