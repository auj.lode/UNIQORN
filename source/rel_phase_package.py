# this file collects routines to extract the relative phases from momentum-space single shots of bosons in double wells
# by Phila Rembold
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import numpy as np
import os
import warnings

def gauss(x,amp,wid):
    # Gauss function
    return amp*np.exp(-x*x*wid)

def Pmodel(x, amp, wid, con, freq, phase):
    # Function to be fitted : Cosine modulated Gaussian
    #
    # amp   = amplitude of Gaussian
    # wid   = 1/sigma^2 (to make sure its about the same order of magnitude as the others)
    # con   = contrast i.e. amplitude of cosine
    # freq  = frequency of cosine
    # phase = phase of cosine
    #
    return amp*np.exp(-x*x*wid)*(1.0+con*np.cos(freq*x+phase))

def initial_fit(xdata, ydata, freq_min, bound, plot=1):
    # Fits xdata (average distribution) and ydata to the model. Created to initially find the right frequency,
    # amplitude and width.
    #
    # xdata = x values (numpy array)
    # ydata = y values (numpy array)
    # freq_min =  minimum acceptable frequency (float)
    # bound = bounds for all the parameters to e optimised (numpy array)
    #

    if len(ydata.shape)>1:
        ydata_av=np.mean(ydata,axis=1)
    else:
        ydata_av=ydata

    # find a good initial frequency guess through fourier transform
    fre_ydata = np.fft.fft(ydata_av)[:int(len(ydata_av)/2)]
    frequ = 2*np.pi/(xdata[1]-xdata[0])*np.linspace(0,1,len(xdata))
    np.argmax(fre_ydata)
    freq_guess=0.0
    for n in range(0,len(fre_ydata-1)):
        pos = np.argmax(fre_ydata[n:])+n
        w = frequ[pos]
        if w<7 and w>freq_min:
            freq_guess=w
            break

    # Only fit to Gaussian first
    gauss_init_vals = [1.,.1]
    [amp, wid], var = curve_fit(gauss, xdata, ydata_av, p0 = gauss_init_vals)

    # Fit to the full model
    init_vals = [amp, wid, 0.9, freq_guess, 0.]  # for [amp, wid, con, freq, phase]
    while True:
        try:
            best_vals, var = curve_fit(Pmodel, xdata, ydata_av, p0 = init_vals, bounds=bound)
            break
        except (RuntimeError,ValueError):
            best_vals = [amp, wid, 0.0, freq_guess, 0.]
            break

    # Calculate number of particles in BEC
    if len(ydata.shape)>1:
        N = np.sum(ydata[:,0])
        title = "Average Distribution in Momentum Space"
        #if best_vals[2]<0.2:
        #    return best_vals
    else:
        N = np.sum(ydata[:])
        title = "Average Distribution in Momentum Space (low contrast)"
        #fig = plt.figure(4)
        #ax = fig.add_subplot(233)
        #ax.set_title(title)
        #title = "Exemplary Single Shot"
        #if best_vals[2]<0.9:
        #    return best_vals

    if plot==0:
        return best_vals
    # Do the plot

    # CONVERSION NEEDS TO BE CHECKED
    dx = 2/(2*len(xdata)) # in um for +-2um
    fourier_conv = 1/(2*dx*(max(xdata)-min(xdata))/2) # in MHz
#    xdata_fit = np.linspace(min(xdata), max(xdata), 1000)
#
#    fig = plt.figure(4)
#    ax = fig.add_subplot(233)
#    ax.clear()
#    ax.plot(xdata*fourier_conv,ydata_av/N, label="Simulated Data")
#    ax.plot(xdata_fit*fourier_conv, model(xdata_fit,best_vals[0],best_vals[1],best_vals[2],best_vals[3],best_vals[4])/N,linestyle='--', label="Best Fit")
#    ax.legend()
#    ax.set_title(title)
#    ax.set_xlabel('Momentum [MHz]')
#    ax.set_ylabel('Normalised Distribution')
#    ax.set_xlim([-50.0,50.0])
    return best_vals

def scatterplot(phases, contrasts):
    # Compute areas and colors, make sure the dots are visible
    area = 10**6 * np.pi*(contrasts[:,1]**2+phases[:,1]**2)
    for n in range(0,len(area)):
        if area[n]<10:
            area[n]=10
        elif area[n]>10000:
            area[n]=10000
    # color is proportional to th standard deviation
    colors = area/max(area)#(contrasts[:,1]**2+phases[:,1]**2)/max((contrasts[:,1]**2+phases[:,1]**2))

    # plot the phase points
    fig = plt.figure(4)
    ax  = fig.add_subplot(231, projection='polar')
    ax.scatter(phases[:,0], contrasts[:,0], s=area, c=colors, cmap='bone', alpha=0.75)
    ax.set_title('Contrast and relative Phase of Single Shots')
    ax.set_ylim([0,1.1])

    # Mark maximum contrast
    max_contrast = plt.Circle((0, 0), 1, transform=ax.transData._b, fill=False, edgecolor='black', linewidth=2, alpha=1, zorder=9)
    ax.add_artist(max_contrast)

    ax.plot()

def circular_var(phases):
    # The circular variance is calculating by averaging all the vectors describing the samples and determining the resulting 
    # length of the vector. However, as this woulf limit the variance to be between 0 and 1, we use the square of the standard 
    # distribution instead.
    R = np.abs(1/len(phases)*sum(np.exp(1j*phases)))
    var_cir = 1 - R
    var = -2*np.log(R)
    return var

def circular_mean(phases):
    # The circular mean is calculating by adding up all the vectors describing the samples and determining the angle
    theta = np.angle(sum(np.exp(1j*phases)))
    mean = theta
    return mean

def fit_plot(mean,var):
    # plots phase and contrast in the polar plane
    mx = mean[0]
    my = mean[1]
    sx = np.sqrt(var[0])
    sy = np.sqrt(var[1])
    rho = var[2]/(sx*sy)

    # Construct normal distribution from variances
    normal = lambda x, y: 1/(2*np.pi*sx*sy*np.sqrt(1-rho**2))*np.exp(-(((x-mx)**2)/sx**2-2*rho*(x-mx)*(y-my)/(sx*sy)+((y-my)**2)/sy**2)/(2*(1-rho**2)))
    rad = np.linspace(0, 1.1, 300)
    azm = np.linspace(mx-np.pi, mx+np.pi, 300)
    r, th = np.meshgrid(rad, azm)

    # make polar plot
    fig = plt.figure(4)
    ax = fig.add_subplot(232, projection='polar')
    ax.pcolormesh(th,r,normal(th,r), cmap='Spectral')
    ax.set_title('Fitted Contrast and Relative Phase Distribution')

    # Mark maximum contrast
    max_contrast = plt.Circle((0, 0), 1, transform=ax.transData._b, fill=False, edgecolor='black', linewidth=2, alpha=1, zorder=9)
    ax.add_artist(max_contrast)

    ax.plot()


def extract_info(filename, freq_min):

    # Locate file
    dir_path = os.path.dirname(os.path.realpath(__file__))
    _INPUT_FILE = os.path.join(dir_path,filename)

    # Import file
    with open(_INPUT_FILE, 'r') as input_file:
        # Read in the x and y values
        lines = input_file.readlines()
        width = len(lines[0].split())
        data = np.zeros((len(lines)-1,width)) #All the collumns of the file
        for i in range(0,len(lines)-1):
            date = lines[i].split()
            data[i,0:width] =np.array(date)

    # Bring data into useful form
    xdata_ini = np.transpose(data[:,0])
    ydata_ini = data[:,3:width]

    # Define Boundaries for the average fit
    bound_av = ([0,0,0,freq_min,-np.pi],[float(np.max(ydata_ini)),float(max(xdata_ini)*2),1.,7., 2.*np.pi]) # for [amp, wid, con, freq, phase]

    # Average to get a initial guess for values -> this might be a bad idea (for distributed phase the fit will be bad by definition)
    [amp_av, wid_av, con_av, freq_av, phase_av] = initial_fit(xdata_ini,ydata_ini, freq_min, bound_av)

    if con_av < 0.2:
        # Try getting the frequency by using different single shots. I use ten at a time and require at least 3 to get a
        # satisfying average. Otherwise it is trying the next 10 and so on.
        freqs=[]
        ss_start=0
        while True:
            for ini in range(ss_start,ss_start+9):
                with warnings.catch_warnings():
                # As the plot over writes itself it produces a warning which will be suppressed
                    warnings.simplefilter("ignore")
                    [amp_av, wid_av, con_av, freq_av, phase_av] = initial_fit(xdata_ini,ydata_ini[:,ini], freq_min,
                                                                              bound_av,plot=0)
                if con_av > 0.9:
                #  to make sure only well fitted shots are used, we only average over them, if the contrast is high
                    freqs.append([amp_av, wid_av, con_av, freq_av, phase_av])
            ss_start = ss_start+10

            # break, if at least 3 single shots with con_av > 0.9 have been found
            if len(np.array(freqs).shape) > 1:
                if len(np.array(freqs)[:,0]) > 2:
                    freqs  = np.array(freqs)
                    break

            # also break, if the list ends or 30 shots aren't enough
            if ((ss_start+9) > len(ydata_ini[0,:])) or (ss_start >= 39):
                # set everything to zero and plot the average wavefunction
                freqs  = np.zeros((1,5))
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    initial_fit(xdata_ini,ydata_ini, freq_min, bound_av)
                break

        # Ensure that outliers don't ruin the averaging
        frequ_median = np.median(freqs[:,3])
        frequ_std    = np.std(freqs[:,3])
        i = 0
        if frequ_std > 0.1:
            while i <= len(freqs)-1:
                if np.abs(freqs[i,3]-frequ_median)>frequ_std:
                    # this ensures outliers are eliminated
                    freqs = np.delete(freqs,i,0)
                    i = i-1
                i=i+1

        # calculate the averages to use as a basis for the following fits
        shots = len(freqs[:,0])
        amp_av   = np.sum(freqs[:,0])/shots
        wid_av   = np.sum(freqs[:,1])/shots
        con_av   = np.sum(freqs[:,2])/shots
        freq_av  = np.sum(freqs[:,3])/shots
        phase_av = np.sum(freqs[:,4])/shots

    if con_av > 0.1:
        # Define new boundaries (dependent on the average) for the direct fits
        bound = ([0,phase_av-np.pi],[1.,phase_av+np.pi]) # for [con, phase]

        # Fit to single data points
        phases    = np.zeros((width-4,3))
        contrasts = np.zeros((width-4,3))

        for m in range(0,width-4):
            best_vals, var = curve_fit(lambda x, con, phase: Pmodel(x, amp_av, wid_av, con, freq_av, phase), xdata_ini,
                                       ydata_ini[:,m], p0 = [con_av, phase_av], bounds=bound)
            phases[m,:]=[best_vals[1], var[1,1], (1/2)**var[1,1]] # [value, variance, weight]
            contrasts[m,:]=[best_vals[0], var[0,0], (1/2)**var[0,0]] # [value, variance, weight]

        #print('PHASES'+str(phases))
        # Plot single data points on polar plot
#        scatterplot(phases, contrasts)

        # Establish a weighting depending on the quality of the fit
        phases_w    = len(phases)/np.sum(phases[:,2])*phases[:,0]*phases[:,2]
        contrasts_w = len(contrasts)/(np.sum(contrasts[:,2]))*contrasts[:,0]*contrasts[:,2]

        # Calculate mean and variance for fitted points
        best_pC = [circular_mean(phases_w),np.mean(contrasts_w)]
        var_pC  = [circular_var(phases_w),np.mean((contrasts_w-best_pC[1])**2),np.mean((contrasts_w-best_pC[1]) *
                                                                                       (phases_w-best_pC[0]))]

        # Plot resulting distribution on polar plot
#        fit_plot(best_pC,var_pC)

        if con_av<0.5:
            print(" WARNING: The contrast in the initial fit is\n below 0.5. The frequency might not be reliable.")
    else:
        print("\n ERROR: The contrast in the initial fit is too\n low to extract a reliable frequency.\n Contrast and phase are set to zero.\n")

        best_pC   = [0,0]
        var_pC    = [0,0]
        phases    = np.zeros((1,3))
        contrasts = np.zeros((1,3))
#        fig = plt.figure(4)
#        with warnings.catch_warnings():
#            warnings.simplefilter("ignore")
#            fig.add_subplot(233).set_visible(False)

    return best_pC, var_pC, phases[:,0], contrasts[:,0]


def plot_phase_histogram(phases, n_bins=15, hist_limits_x=None, hist_limits_y=None):
    fig = plt.figure(4)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        ax=fig.add_subplot(231)
        ax.remove()
        ax=fig.add_subplot(231)
        ax.hist(phases, density=1, bins=n_bins)
        ax.set_title('Phase Histogram')
        ax.set_xlabel('Phase [rad]')
        ax.set_ylabel('Normalised Distribution')
        if hist_limits_x!=None:
            ax.set_xlim(hist_limits_x)
        if hist_limits_y!=None:
            ax.set_ylim(hist_limits_y)
        if len(phases)<4:
            ax.clear()
    return
