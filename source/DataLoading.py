# This file collects the routines that get the labels (single shots, particle number or fragmentation) 
# from files

## inputs::
## inp.Training_Data -> string that defines the dataset
## inp.Learn -> string that defines the labels
## inp.DirName -> String containing the prefix used for data generation 
## inp.NShots -> integer, the number of single shots to extract from each of the directories
## inp.NDatasets -> integer, the number of datasets, i.e., the number of directories with the supplied prefix
## inp.Npoints -> integer,  the number of points in the MCTDH-X computations or single-shot simulations
## inp.Randomize -> boolean default 'False', shuffle the single shots

import numpy as np
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.axes as ax
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import subprocess
import os
import sys
from functions import *
import random
import Input as inp
import rel_phase_package 
# -*- coding: utf-8 -*-

####################################################################
# IMPORT DATASET AND LABELS
####################################################################


####################################################################
def data_ID_loader(**kwargs):
    """Loads the paths (IDs) for the dataset and already splits them into training and test sets.
    
      Parameters
      ----------
      **kwargs :

          ----------
          - data (=inp.Training_Data) : str
          specifies what is the data, can be any of the following:
           
          * FRAG :
              Flag to extract the fragmentation

          * NPAR :
              Flag to extract the number of particles
        
          * DENS:
              Flag to extract the density

          * CORR1/CORR2/RHO1/RHO2:
              Flag to extract the one-body or two-body reduced density matrix (RHO) or Galuber correlations (CORR)

          * POT:
              Flag to extract the potential

          * PHASEHIST:
              Flag to extract the phase histogram


      Returns
      -------

      (IDs_training, IDs_test)

      The paths where the data sets for training and test are stored.

        

        References
        ----------

        See Also
        --------

        Notes
        -----

        Examples
        --------

    """
    #input variables:
    #NDatasets=int(inp.NDatasets)
    
    
    #getting the information about which observables will be used as independent variables (data)
    #and which as dependent ones (aka "labels", y-values,...).
    #Default are data='SSS' and labels='Dens'.
    data=kwargs.get('data','SSS')
    labels=kwargs.get('labels','Dens')

    #Setting different file names for different data to be extracted as it is saved in separate files:
    #data:
    if data=='SSS' or data=='NPAR':
        DataFileName='SingleShots.dat'
    if data=='DENS':
        DataFileName='-density.dat'
    if data=='CORR1' or data=='CORR2' or data=='RHO1' or data=='RHO2':
        DataFileName='-correlations.dat'
    if data=='POT':
        DataFileName='orbs.dat'
    if inp.momData==True:
        DataSpaceStr='k'
    else:                           # Real space data
        if data=='PHASEHIST':
            print("Cannot get a phase histogram from real-space single shots")
            sys.exit()
        DataSpaceStr='x'

    #Now we have our complete paths where to retrieve data:
    IDs_tot = glob.glob(inp.DirName+'*/*'+DataSpaceStr+DataFileName)
    
    #verify if IDs are correct:
    #print(IDs_tot)
    
    #select only the number of IDs needed for the training/testing:
    random.shuffle(IDs_tot)  #reshuffle again
    #IDs_tot.sort()
    
    if inp.DataThreshold:
        IDs = DataThresholdSelection(IDs_tot,inp.DataThresholdLow,inp.DataThresholdHigh,int(inp.NDatasets))
    else:
        IDs = IDs_tot[:int(inp.NDatasets)]
    #print('Folders used for the training/testing:',IDs)

    #Now we want to split the data such that the test set stems entirely
    #from wavefunctions not used for the training:
    length_IDs = len(IDs)
    length_IDs_training = int(np.ceil(inp.TrainFraction*length_IDs))
    #print('Number of total data sets', length_IDs)
    #print('Number of training data sets:', length_IDs_training)
    #print('Number of testing data sets:', length_IDs - length_IDs_training)
    IDs_training = IDs[:length_IDs_training]
    IDs_test = IDs[length_IDs_training:]
    # if we do not activate the threshold in the test set,
    # we now remix the test set with the data discarded in the previous step
    if inp.DataThreshold and (not inp.DataThresholdTest):        
        IDs_discarded = list(set(IDs_tot)-set(IDs))
        IDs_test.extend(IDs_discarded)
        random.shuffle(IDs_test)
        IDs_test = IDs_test[:length_IDs - length_IDs_training]
    
    #data has to be fed as numpy array:
    #IDs_training = np.array(IDs_training)
    #IDs_test = np.array(IDs_test)
    
    # if usage multiple configurations of single-shot is activated,
    if inp.MultiSSSOrder:
        IDs_temp = IDs_training.copy()
        for iconfigs in range(inp.MultiSSS_NOrder-1):
            IDs_training.extend(random.sample(IDs_temp,len(IDs_temp)))
            
    # If flipping along the position/momentum axis is activated,
    # The training data will be duplicated.
    # Later in the method "extractor", the second half (the duplicated) data will be flipped.
    if inp.AxisFlipping:
        IDs_training.extend(random.sample(IDs_training,len(IDs_training)))
    
    print('Number of total data sets in use:', length_IDs)
    print('Number of data sets used for training:', length_IDs_training)
    print('Number of training data sets after duplication:', len(IDs_training))
    print('Number of testing data sets:', length_IDs - length_IDs_training)
    
    return IDs_training, IDs_test
####################################################################




####################################################################
def label_ID_loader(DataNameComplete, **kwargs):
    """Loads the paths (IDs) for the dataset and already splits them into training and test sets.
    
      Parameters
      ----------
      **kwargs :

      ----------
      - labels (=inp.Learn) : str
        specifies what is the data, can be any of the following:
           
          * FRAG :
              Flag to extract the fragmentation

          * NPAR :
              Flag to extract the number of particles
        
          * DENS:
              Flag to extract the density

          * CORR1/CORR2/RHO1/RHO2:
              Flag to extract the one-body or two-body reduced density matrix (RHO) or Galuber correlations (CORR)

          * POT:
              Flag to extract the potential

          * PHASEHIST:
              Flag to extract the phase histogram


      Returns
      -------

      LabelNameComplete:

      The paths where the labels of the corresponding data sets (for training or testing) are stored.

        

        References
        ----------

        See Also
        --------

        Notes
        -----

        Examples
        --------

    """
    
    #getting the information about which observables will be used as dependent ones (aka "labels", y-values,...).
    #Default is labels='Dens'.
    labels=kwargs.get('labels','Dens')
    
    #define correct labels:
    if inp.momLabels==True:
        LabelSpaceStr='k'
    else:
        LabelSpaceStr='x'
    if labels=='SSS':
        LabelNameComplete=glob.glob('/'.join(DataNameComplete.split('/')[:-1])+'/*'+LabelSpaceStr+'SingleShots.dat')
    if labels=='NPAR':
        LabelNameComplete=DataNameComplete
    if labels=='DENS':
        LabelNameComplete=glob.glob('/'.join(DataNameComplete.split('/')[:-1])+'/*'+LabelSpaceStr+'-density.dat')
    if labels=='CORR1' or labels=='CORR2' or labels=='RHO1' or labels=='RHO2':
        LabelNameComplete=glob.glob('/'.join(DataNameComplete.split('/')[:-1])+'/*'+LabelSpaceStr+'-correlations.dat')
    if labels=='PhaseHist':
        LabelNameComplete=glob.glob('/'.join(DataNameComplete.split('/')[:-1])+'/*'+LabelSpaceStr+'SingleShots.dat')
    if labels=='FRAG':
        LabelNameComplete='/'.join(DataNameComplete.split('/')[:-1])+'/NO_PR.out'
    if labels=='POT':
        LabelNameComplete=DataNameComplete.split('N')[0]+'orbs.dat'

    #Note that this function is called for sets which have already been chosen
    #as training or test!
    return LabelNameComplete
####################################################################










####################################################################
def extractor(WhichData,DataFileName,K):
    #print('Entering extractor:'+WhichData+DataFileName)
    # initialize return variable
    ThisData=[]

####################################################################
####  extract single shots
####################################################################
    if WhichData=='SSS':
        #open and read content of the file (it will be automatically closed too)
        with open(DataFileName) as f:
            content = f.readline()
            no_of_columns = len(content.split('     '))
            #print('no_of_columns',no_of_columns)
            if (inp.NShots or 0) > 0 and (inp.NShots or 0) <= no_of_columns - 3:
                pass
            else:
                inp.NShots = no_of_columns - 3

        # get single shots from this dataset in ThisData
        #'''Read file as np.array, drop first 3 columns and transpose'''
        
        if inp.MultiSSSOrder:
            ThisData = np.transpose(np.loadtxt(DataFileName, usecols=np.arange(3,no_of_columns)))
            ThisData = ThisData[np.random.permutation(np.arange(len(ThisData)))]
            ThisData = ThisData[:inp.NShots]
        else:
            ThisData = np.transpose(np.loadtxt(DataFileName, usecols=np.arange(3,inp.NShots+3)))

####################################################################
####  extract particle number
####################################################################
    if WhichData=='NPAR':
        # if 'NPAR', then extract the particle number as labels
        Ntot = DataFileName.split('00N')[1].split('M')[0]
        try: Ntot = int(Ntot)
        except ValueError:
            print('Warning: No integer for total particle number found.')

        ThisData = Ntot

####################################################################
####  extract density
####################################################################
    if WhichData=='DENS':
        ThisData = np.transpose(np.loadtxt(DataFileName, usecols=(3,)))
####################################################################
####  extract correlations
####################################################################
    # calculate/extract the correlation functions or reduced density matrix form 
    # MCTDHX data
    if (WhichData=='CORR1'): # Glauber one-body correlation function (absolute value squared)
        with open(DataFileName, 'r') as CorrFile:
            ThisData=np.loadtxt(CorrFile, usecols=(7,8,6,9))
        CorrFile.close()
        ThisData=(ThisData[:,0]**2+ThisData[:,1]**2)/(ThisData[:,2]*ThisData[:,3])
        ThisData=np.nan_to_num(ThisData.transpose(),nan=0.0,posinf=0.0,neginf=0.0)
    if (WhichData=='CORR2'): # Glauber two-body correlation function
        with open(DataFileName, 'r') as CorrFile:
            ThisData=np.loadtxt(CorrFile, usecols=(10,6,9))
        CorrFile.close()
        ThisData=(ThisData[:,0])/(ThisData[:,1]*ThisData[:,2])
        ThisData=np.nan_to_num(ThisData.transpose(),nan=0.0,posinf=0.0,neginf=0.0)
    if (WhichData=='RHO1'): # One-body reduced density matrix (absolute value squared)
        with open(DataFileName, 'r') as CorrFile:
            ThisData=np.loadtxt(CorrFile, usecols=(7,8))
        CorrFile.close()
        ThisData=(ThisData[:,0]**2+ThisData[:,1]**2)
        ThisData=ThisData.transpose()
    if (WhichData=='RHO2'): # Two-body reduced density matrix
        with open(DataFileName, 'r') as CorrFile:
            ThisData=np.loadtxt(CorrFile, usecols=(10))
        CorrFile.close()
        ThisData=ThisData.transpose()

####################################################################
####  extract fragmentation
####################################################################
    if WhichData=='FRAG':
        with open(DataFileName, 'r') as f:
            lines = f.read().splitlines()
            last_line = lines[-1]

        FRAG = last_line.split('      ')[1].strip()
        FRAG = "{:.16f}".format(float(FRAG))
        FRAG = float(FRAG)
        ThisData=FRAG

####################################################################
####  extract phase histograms
####################################################################
    if WhichData=='PHASEHIST':
        #extract_info(DataNameComplete,1.0)
    #     print('SHOTS:::'+str(DataNameComplete))
        _, _, ThisData, _= rel_phase_package.extract_info(DataFileName,1.0)
        ThisData=histogrammer(ThisPhaseHist,-np.pi,np.pi,inp.PhaseBins)

####################################################################
####  extract potential
####################################################################
    if WhichData=='POT':
        #for every stack of single shots, extract the potential form the
        #values of the MCTDHX-computed data
        with open(DataFileName, 'r') as PotFile:
            ThisData = np.loadtxt(PotFile, usecols=[4],skiprows=2)
        PotFile.close()
        ThisData = ThisData.transpose()

    #Extract x-boundaries and grid for later and save them into a file:
    if (K==0 and WhichData=='POT' or WhichData=='DENS'):

        if WhichData=='POT':
            toskip=2
        else:
            toskip=0

        Xmin=str(min(np.loadtxt(DataFileName, usecols=[0],skiprows=toskip)))
        Xmax=str(min(np.loadtxt(DataFileName, usecols=[0],skiprows=toskip)))
        
        file_x_boundaries=open('Xboundaries.txt', 'w+')
        file_x_boundaries.write(Xmin+"\n")
        file_x_boundaries.write(Xmax)
        file_x_boundaries.close()

        VizGrid=np.loadtxt(DataFileName, usecols=[0],skiprows=toskip)
        np.savetxt('VizGrid.txt',np.reshape(VizGrid,(-1,1)))

    if (K==0 and WhichData=='RHO1' or WhichData=='RHO2' or WhichData=='CORR1' or WhichData=='CORR2'):
        VizGrid=np.loadtxt(DataFileName, usecols=[0,3])
        np.savetxt('VizGrid.txt',np.reshape(VizGrid,(-1,2)))

        
    return ThisData
####################################################################




####################################################################
def DataThresholdSelection(IDs_tot,Low,High,NDatasets):
    # This method will be called only when Learn = 'FRAG' or 'NPAR'
    
    RemoveItems = np.zeros(len(IDs_tot),dtype=int)
    IDs_new = np.zeros(int(inp.NDatasets))
    
    for i, DataNameComplete in enumerate(IDs_tot):

        #self-consistent checking that DataNameComplete is not a list:
        if isinstance(DataNameComplete,list):
            DataNameComplete=str(DataNameComplete[0])

        #load label filename based on current data directory/path, via subroutine in DataLoading.py:
        LabelNameComplete = label_ID_loader(DataNameComplete, labels=inp.Learn)
        #self-consistency check:
        if isinstance(LabelNameComplete,list):
            LabelNameComplete=str(LabelNameComplete[0])
        # extract label from the file using extractor subroutine in DataLoading.py:
        ThisLabel=extractor(inp.Learn,LabelNameComplete,i)
        
        
        if (ThisLabel>Low and ThisLabel<High):
            RemoveItems[i] = 1  # These data will be discarded

            
    NumLeft = len(RemoveItems) - np.sum(RemoveItems)
    IDs_tot = [IDs_tot[i] for i in range(len(IDs_tot)) if RemoveItems[i]==0]
    
    # If the available data which satisfy the criterion imposed by threshold selection
    # is fewer than the desired NDatasets,
    # We will repeat the data
    
    if NumLeft >= int(inp.NDatasets):
        IDs_new = IDs_tot[:int(inp.NDatasets)]
    elif NumLeft < int(inp.NDatasets):
        for i in range(int(inp.NDatasets)):
            IDs_new[i] = IDs_tot[i%NumLeft]
        print('The available data satisfying the threshold criterion is fewer than the desired NDatasets')
        print('Duplicated data will be used. Data duplicated ',i//NumLeft+1,' times.')
        
    random.shuffle(IDs_new)
    
    return IDs_new

