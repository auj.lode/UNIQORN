# This file collects runtime checks for the python environment
import Input as inp
import importlib
import sys
import glob

def runtime_checks():
    """ Perform runtime checks on python environment """
##################################################################################################################
#########            RUNTIME CHECKS ON PACKAGES            #########
#########################################################

    if sys.version_info.major != 3:
        print('Please use python3.')
        sys.exit()
    if importlib.util.find_spec("numpy") is None:
        print('Missing package: numpy')
        sys.exit()
    if importlib.util.find_spec("matplotlib") is None:
        print('Missing package: matplotlib')
    if importlib.util.find_spec("scipy") is None:
        print('Missing package: scipy')
        sys.exit()
    if importlib.util.find_spec("tensorflow") is None:
        print('Missing package: tensorflow')
        sys.exit()
    if importlib.util.find_spec("tqdm") is None:
        print('Missing package: tqdm')
        print('Progress bars are not activated.')
    if (importlib.util.find_spec("keras")) is None:
        print('Missing package: tensorflow.keras and keras')
        sys.exit()

    import tensorflow as tf
    tfVERSION = tf.version.VERSION
    if int(tfVERSION[0]) == 1:
        print('Please upgrade your tensorflow to version >= 2.2.0.')
        sys.exit()
    if (int(tfVERSION[0]) == 2) and (int(tfVERSION[2]) < 2):
        print('We recommend you to upgrade your tensorflow to version >= 2.2.0.')

    import numpy
    import scipy
    import matplotlib
    pythonVERSION = str(sys.version_info.major)+'.'+str(sys.version_info.minor)+'.'+str(sys.version_info.micro)
    numpyVERSION = numpy.__version__
    matplotlibVERSION = matplotlib.__version__
    scipyVERSION = scipy.__version__
    print('----------------------------------------------------')
    print('All the required python packages are available.')
    print('VERSIONS: python '+pythonVERSION+', numpy '+numpyVERSION+', scipy: '+scipyVERSION+', matplotlib: '+matplotlibVERSION+', tensorflow: '+tfVERSION+'.')
    print('----------------------------------------------------')


##################################################################################################################
#########            RUNTIME CHECKS ON INPUT PARAMETERS            #########
#########################################################
# some runtime checks on the input
    """
    Perform runtime checks for the consistency of the structure of the input data.
    
    Parameters
    ----------
    NONE
    
    Returns
    -------
    NONE, executes runtime checks and aborts if there are inconsistencies.
    
    References
    ----------

    See Also
    --------

    Notes
    -----

    Examples
    --------
    
    """

    # some runtime checks on the input
    NTrainDatasets=inp.NDatasets*inp.TrainFraction
    NTestDatasets=inp.NDatasets*(1-inp.TrainFraction)
    
    if (inp.TrainFraction > 1) or (inp.TrainFraction < 0):
        print('The training fraction is '+str(inp.TrainFraction)+' but it should be a number between 0 and 1! Good choice: 0.8.')
        sys.exit()
    
    if (not NTestDatasets.is_integer()):
        print('Non-integer number of testing data sets '+str(NTestDatasets)+' . Please adjust.')
        sys.exit()
    elif (not NTrainDatasets.is_integer()):
        print('Non-integer number of training data sets '+str(NTrainDatasets)+' . Please adjust.')
        sys.exit()

    if (inp.NShots % inp.NShotsPerSample != 0):
        print('Number of single shots per sample '+str(inp.NShotsPerSample)+' and number of single shots per wave function '+str(inp.NShots)+' are not compatible. Please adjust.')
        sys.exit()
        
    if (NTrainDatasets % inp.batch_size != 0):
        print('The batch size '+str(inp.batch_size)+' is not compatible with the number of wave functions '+str(inp.NDatasets*inp.TrainFraction)+' used in training. Please adjust.')
        sys.exit()

    elif (NTestDatasets % inp.batch_size != 0):
        print('The batch size '+str(inp.batch_size)+' is not compatible with the number of wave functions '+inp.NDatasets*(1-inp.TrainFraction)+' used in testing. Please adjust.')
        sys.exit()

    if ((inp.DataThreshold) and (not (inp.Learn=='FRAG' or inp.Learn=='NPAR'))):
        print('Selection of data threshold applies only for learning of FRAG or NPAR.')
        sys.exit()

    if inp.MultiSSSOrder:
        if not inp.Training_Data=='SSS':
            print('MultiSSSOrder applies only for Training_Data of \'SSS\'')
            sys.exit()
        if inp.MultiSSS_NOrder == 1 or (not isinstance(inp.MultiSSS_NOrder, int)):
            print('MultiSSS_NOrder needs to be an integer greater than 1.')
            sys.exit()
            
    print('----------------------------------------------------')
    print('Input parameters checked. No inconsistency detected.')
    print('----------------------------------------------------')
    

##################################################################################################################
#########            RUNTIME CHECKS ON DATA            #########
#########################################################

    IDs_tot = glob.glob(inp.DirName+'*/')
    
    print('----------------------------------------------------')
    if len(IDs_tot)==0:
        print('There is no available data, or the given path for data is incorrect.')
        print('----------------------------------------------------')
        sys.exit()
    else:
        print('Number of available data (folders): ',len(IDs_tot))
        print('----------------------------------------------------')
    
    return
##################################################################################################################


