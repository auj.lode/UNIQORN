# This file collects python functions that define ML or other models.
# The return value is a 'model' object (currently, only Keras models).


#########################################################
#########                 IMPORTS               #########
#########################################################
#portability imports:
from __future__ import print_function

import numpy as np
import sys

#machine learning packages (might need to add "tensorflow." in front of "keras.(...)" depending on the version):
import tensorflow as tf
import tensorflow.keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.utils import plot_model
from tensorflow.keras.layers import Dense, Dropout,Conv2D,MaxPooling2D,Flatten, BatchNormalization, LeakyReLU, Conv2DTranspose,AveragePooling2D
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras import regularizers

#some options
np.set_printoptions(threshold=sys.maxsize)

import Input as inp

##################################################################################################################

def clear():

    # To clear computation graph
    tensorflow.keras.backend.clear_session()

##################################################################################################################
# DEFAULT NEURAL NETWORK STRUCTURES
##################################################################################################################
def default():

    print('________________________________________________________________________')
    print('-------------')
    print('Default model construction')
    print('-------------')

    NNSize=inp.Npoints*inp.NShotsPerSample
    print("In Models NNSize, Npoints,NShotsPerSample:"+str(NNSize)+" "+str(inp.Npoints)+" "+str(inp.NShotsPerSample))
    #------------------------------------------------------
    # default MLP model for regression of scalar/vector/matrix quantities
    #-------------------------------------------------------
    if  inp.ConvNet==False:
        print('Building a default Multi-Layer Perceptron model!')
        # build keras NN model
        model = Sequential()
        model.add(Dense(512, activation='relu', input_dim=NNSize, kernel_initializer='glorot_normal'))
        model.add(Dense(256, activation='relu', kernel_initializer='glorot_normal',
                                                  kernel_regularizer=regularizers.l2(0.3)))
        model.add(Dense(128, activation='relu', kernel_initializer='glorot_normal',
                                                  kernel_regularizer=regularizers.l2(0.1)))
      
    #----------------------------
    # default CNN model
    #----------------------------
    elif inp.ConvNet==True:
          print('Building and training a default Convolution Neural Network model with kernel size=2!')
          
          kernel_size=2
          model = Sequential()
          # [Conv. + Relu] block
          model.add(Conv2D(128,
                           (kernel_size, kernel_size),
                           strides=1,
                           input_shape=(inp.Npoints,inp.NShotsPerSample,1),
                           activation='relu',
                           padding='same',
                           kernel_initializer='glorot_normal'))

          model.add(Conv2D(128,
                           (kernel_size, kernel_size),
                           #(2, 2),
                           kernel_initializer='glorot_normal',
                           activation='relu'))

          # [Flatten (i.e. convert to vector) + fully connected] block
          model.add(MaxPooling2D(pool_size=(2, 2)))

          model.add(Conv2D(64,
                           (kernel_size, kernel_size),
                           #(2, 2),
                           activation='relu',
                           kernel_initializer='glorot_normal'))
    #      model.add(MaxPooling2D(pool_size=(2, 2)))
          model.add(Flatten())
    #      model.add(BatchNormalization())
          model.add(Dense(64,
                          activation='relu',
                          kernel_initializer='glorot_normal'))
    # different output layers according to task
    if (inp.Learn=='DENS' or inp.Learn=='POT'):
       model.add(Dense(inp.Npoints))
    elif (inp.Learn=='PHASEHIST'):
       model.add(Dense(inp.PhaseBins))
    elif (inp.Learn=='FRAG' or inp.Learn=='NPAR'):
       model.add(Dense(1))
    elif (inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO2' or inp.Learn=='RHO1'):
       model.add(Dense(inp.Npoints**2))
          
    print('________________________________________________________________________')
    return model
##################################################################################################################
   
   
   
   
   
   
##################################################################################################################
# CUSTOM NEURAL NETWORK STRUCTURES
##################################################################################################################
def custom(**kwargs):

    print('________________________________________________________________________')
    print('-------------')
    print('Custom model construction')
    print('-------------')
    # define size of single data set (input layer size)
    NNSize=inp.Npoints*inp.NShotsPerSample
    print("In Models NNSize, Npoints,NShotsPerSample:"+str(NNSize)+" "+str(inp.Npoints)+" "+str(inp.NShotsPerSample))
    # To clear computation graph
    #keras.backend.clear_session()
    
    #----------------------------
    # custom MLP model
    #----------------------------
    if (inp.ConvNet==False):

      # check if layers were input
      if hasattr(inp, 'layers'):
        layers=inp.layers
      else: # if not, use default structure
        layers=[512,256,128,64]

      print("layer structure in custom MLP:"+ str(layers))
        
      # check if regularizations were provided
      if hasattr(inp, 'regularizations'):
        regularizations=inp.regularizations
      else: # if not, use default structure
        regularizations=[]
        reg=1.0
        for j in range(len(layers)):
            regularizations.append(reg)
            reg=reg/10.0
      # write 
      print("regularizations in custom MLP:"+str(regularizations))
             
      model = Sequential()
        
      for j in range(len(layers)):  # loop through a stack of dense layers and assemble model
          model.add(Dense(layers[j], 
                    activation='selu', 
                    input_dim=NNSize, 
                    kernel_initializer='glorot_normal', 
                    kernel_regularizer=regularizers.l2(regularizations[j])))


    #----------------------------
    # custom CNN model
    #----------------------------
    if (inp.ConvNet==True):
      # check if layers were input
      if hasattr(inp, 'filters'):
        filters=inp.filters
      else: # if not, use default structure
        filters=[128,128,64]

      if hasattr(inp, 'kernelsizes'):
         kernelsizes=inp.kernelsizes
      else:
         kernelsizes=[(1,int(inp.NShotsPerSample/2)),(32,2),(32,1)]

      if hasattr(inp, 'pooling'):
         pooling=inp.pooling
      else:
         pooling=['max','none','max']

      if hasattr(inp, 'batchnormalization'):
         batchnormalization=inp.batchnormalization
      else:
         batchnormalization=[True,False,False]

      if (inp.InterpolatePredictions==True):
        initializer = tf.keras.initializers.RandomUniform(minval=-5., maxval=5.)
      else:
        initializer = tf.keras.initializers.GlorotNormal()

      print("Number of filters in custom CNN:"+ str(filters))

      model = Sequential()

      model.add(Conv2D(filters[0],
                       kernelsizes[0],
                       strides=1,
                       input_shape=(inp.NShotsPerSample,inp.Npoints,1),
                       activation='selu',
                       padding='same',
                       kernel_initializer=initializer))
      if (pooling[0]=='max'):
          model.add(MaxPooling2D(pool_size=kernelsizes[0],padding='same'))
      if (pooling[0]=='avg'):
          model.add(AveragePooling2D(pool_size=kernelsizes[0],padding='same'))
          
      if (batchnormalization[0]==True):
         model.add(BatchNormalization())

      for j in range(len(filters)-1):  # loop through a stack of dense layers and assemble model
          print("looping through layer "+str(j)+" of custom CNN")
          model.add(Conv2D(filters[j+1],
                           kernelsizes[j+1],
                           strides=1,
                           activation='selu',
                           padding='same',
                           kernel_initializer=initializer))

          if (pooling[j+1]=='max'):
              model.add(MaxPooling2D(pool_size=kernelsizes[j+1],padding='same'))
          if (pooling[j+1]=='avg'):
              model.add(AveragePooling2D(pool_size=kernelsizes[j+1],padding='same'))
          if (batchnormalization[j+1]==True):
             model.add(BatchNormalization())

    #----------------------------
    # Output layer
    #----------------------------

    model.add(Flatten())    
    model.add(Dropout(inp.OutputDropout))
     # add an output layer to the model, depending on the task
    if (inp.Job_Type=='SUPERV_REGR') and (inp.InterpolatePredictions==False):
       if (inp.Learn=='FRAG' or inp.Learn=='NPAR'):
          model.add(Dense(1))
       elif (inp.Learn=='DENS' or inp.Learn=='POT'):
#          model.add(Dense(inp.Npoints,activation='tanh'))
          model.add(Dense(inp.Npoints))
       elif (inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO2' or inp.Learn=='RHO1'):
          model.add(Dense(inp.Npoints**2))
       elif (inp.Learn=='PHASEHIST'):
          model.add(Dense(inp.PhaseBins))

    if (inp.Job_Type=='SUPERV_REGR') and (inp.InterpolatePredictions==True):
       if (inp.Learn=='FRAG' or inp.Learn=='NPAR'):
          print("Cannot generate interpolations for scalars! InterpolatePredictions==True for 'FRAG' and 'NPAR'???")
          exit()
       elif (inp.Learn=='DENS' or inp.Learn=='POT'):
          if (inp.LearnInterpolationKnots==True):
              model.add(Dense(2*inp.InterpolNpoints))
          else:
              model.add(Dense(inp.InterpolNpoints))
       elif (inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO2' or inp.Learn=='RHO1'):
          if (inp.LearnInterpolationKnots==True):
              model.add(Dense(2*inp.InterpolNpoints+inp.InterpolNpoints**2))
          else:
              model.add(Dense(inp.InterpolNpoints**2))
       elif (inp.Learn=='PHASEHIST'):
          if (inp.LearnInterpolationKnots==True):
              model.add(Dense(2*inp.InterpolNpoints))
          else:
              model.add(Dense(inp.InterpolNpoints))

         
    return model


##################################################################################################################
    
    
    
    
    
##################################################################################################################
# NEURAL NETWORK STRUCTURES IN DATABASE
##################################################################################################################
def archive(Model_Name):

    print('________________________________________________________________________')
    print('-------------')
    print('Archive model construction')
    print('-------------')
    # define size of single data set (input layer size)
    NNSize=inp.Npoints*inp.NShotsPerSample
    print("In Models NNSize, Npoints,NShotsPerSample:"+str(NNSize)+" "+str(inp.Npoints)+" "+str(inp.NShotsPerSample))
    
    if Model_Name=='Seq128Dropout':
        
        # build keras NN model
        model = Sequential()
        # Dense layer
        model.add(Dense(128, activation='relu', input_dim=NNSize, kernel_initializer='glorot_normal'))
        # Dropout regularization
        model.add(Dropout(0.))
        # Dense layer
        model.add(Dense(128, activation='relu', kernel_initializer='glorot_normal'))
        # Dropout regularization
        model.add(Dropout(0.))
        # Dense layer
        model.add(Dense(80, activation='relu', kernel_initializer='glorot_normal'))
        # Dropout regularization
        model.add(Dropout(0.))
        # Dense layer
        # model.add(Dense(4, activation='relu', kernel_initializer='glorot_normal'))
        # Dense output layer
        model.add(Dense(1))
    
    
    if Model_Name=='DensityRegressionMLP':
    
        #-------------------------
        # train and test the model
        #-------------------------
        layers=[512,256,128,64]
        regularizations=[0.0,0.3,0.1,0.01]

        #----------------------------
        # custom MLP model
        #----------------------------
        
        model = Sequential()
        
        for j in range(len(layers)):  # loop through a stack of dense layers
           model.add(Dense(layers[j], activation='relu', input_dim=NNSize,
                           kernel_initializer='glorot_normal',
                           kernel_regularizer=regularizers.l2(regularizations[j])))
        model.add(Dense(Npoints))
        
        
        
    if Model_Name=='DensityRegressionCNN':
    
        if inp.ConvNet==False:
            print('This model is a CNN and requires ConvNet=True!')
            exit()
            
        kernel_size=2
        model = Sequential()
        # [Conv. + Relu] block
        model.add(Conv2D(128, (kernel_size, kernel_size),strides=1, input_shape=(inp.Npoints,inp.NShotsPerSample,1), activation='relu',padding='same',kernel_initializer='glorot_normal'))

        model.add(Conv2D(128,(kernel_size, kernel_size),kernel_initializer='glorot_normal',activation='relu'))
        # [Flatten (i.e. convert to vector) + fully connected] block
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(64,(kernel_size, kernel_size),activation='relu',kernel_initializer='glorot_normal'))
        #      model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        #      model.add(BatchNormalization())
        model.add(Dense(64,activation='relu',kernel_initializer='glorot_normal'))
        #      model.add(Dropout(0.3))
        # Dropout regularization
        model.add(Dense(inp.Npoints))

    if Model_Name=='DensityRegressionCNN2':
    
        if inp.ConvNet==False:
            print('This model is a CNN and requires ConvNet=True!')
            exit()
        kernel_size_x=32
        kernel_size_y=int(inp.NShotsPerSample/2)
        model = Sequential()
        # [Conv. + Selu] block
        model.add(Conv2D(32,
                         (1, kernel_size_y),
                         strides=1,
                         activation='selu',
                         padding='same',
                         input_shape=(inp.Npoints,inp.NShotsPerSample,1), 
                         kernel_initializer='glorot_normal'))
          
        model.add(MaxPooling2D(pool_size=(1, kernel_size_y)))
        model.add(Conv2D(32,
                         (kernel_size_x,1),
                         strides=1,
                         activation='softplus',
                         kernel_initializer='glorot_normal'))        
        model.add(Conv2D(32,
                         (kernel_size_x,1),
                         strides=1,
                         activation='softplus',
                         kernel_initializer='glorot_normal'))        
          
#        model.add(MaxPooling2D(pool_size=(int(kernel_size_x), 1)))
        model.add(MaxPooling2D(pool_size=(int(kernel_size_x/4), 2)))
        model.add(Flatten())
        model.add(Dropout(0.25))
        model.add(Dense(inp.Npoints))

    if Model_Name=='CorrelationRegressionCNN1':
    
        if inp.ConvNet==False:
            print('This model is a CNN and requires ConvNet=True!')
            exit()
        kernel_size=2
        model = Sequential()
        # [Conv. + Relu] block
        model.add(Conv2D(128,
                         (kernel_size, kernel_size),
                         strides=1,
                         input_shape=(inp.Npoints,inp.NShotsPerSample,1),
                         activation='relu',
                         padding='same',
                         kernel_initializer='glorot_normal'))
        model.add(BatchNormalization())
        model.add(Conv2D(128,
                         (kernel_size, kernel_size),
                         #(2, 2),
                         kernel_initializer='glorot_normal',
                         activation='relu'))

        # [Flatten (i.e. convert to vector) + fully connected] block
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(64,
                         (kernel_size, kernel_size),
                         #(2, 2),
                         activation='relu',
                         kernel_initializer='glorot_normal'))

        model.add(Flatten())
        model.add(Dense(64,
                        activation='relu',
                        kernel_initializer='glorot_normal'))
        # different output layers according to task
        model.add(Dense(inp.Npoints**2))
          
          
    if Model_Name=='FragmentationRegressionCNN':
      
        if inp.ConvNet==False:
            print('This model is a CNN and requires ConvNet=True!')
            exit()
              
        kernel_size=2
        model = Sequential()
        # [Conv. + Relu] block
        model.add(Conv2D(128, (kernel_size, kernel_size),strides=1, input_shape=(inp.Npoints,inp.NShotsPerSample,1), activation='relu',padding='same',kernel_initializer='glorot_normal'))

        model.add(Conv2D(128,(kernel_size, kernel_size),kernel_initializer='glorot_normal',activation='relu'))
          # [Flatten (i.e. convert to vector) + fully connected] block
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(64,(kernel_size, kernel_size),activation='relu',kernel_initializer='glorot_normal'))
          #      model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
          #      model.add(BatchNormalization())
        model.add(Dense(64,activation='relu',kernel_initializer='glorot_normal'))
          #      model.add(Dropout(0.3))
          # Dropout regularization
        model.add(Dense(1))


          
          
    return model
##################################################################################################################
