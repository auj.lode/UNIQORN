#!/bin/bash
# This script parses all python files in UNIQORN for a provided string
if [ "$#" -ne 1 ]; then
    echo "usage: MLG.sh <string to parse all .py files for>"
fi
grep -Hirn $1 *.py 
