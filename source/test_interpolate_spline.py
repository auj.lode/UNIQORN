# file collects some functions to test interpolate_spline of tensorflow_addons
from tensorflow_addons.image import interpolate_spline
import tensorflow.keras.backend as K
import tensorflow as tf
import numpy as np
import Input as inp
import matplotlib.pyplot as plt # for plotting history etc

# Apply the learned interpolation to the output to generate output for visualization
def ApplyInterpolation(y_data,y_data_interpolation):

    xnew=tf.linspace(np.float32(0),np.float32(inp.Npoints-1),inp.Npoints)
    xnew=tf.expand_dims(xnew,0)
    xnew=tf.expand_dims(xnew,2)

    x,z=tf.split(y_data,2,axis=0)
    x=tf.expand_dims(x,2)
    z=tf.expand_dims(z,2)


    print("shape of x:::", str(x.get_shape().as_list()))
    
    print("x:::",str(x))
    print("shape of xnew:::", str(xnew.get_shape().as_list()))
    print("xnew:::",str(xnew))
    print("shape of z:::", str(z.get_shape().as_list()))
    print("z:::",str(z))


    y_data_interpolation=interpolate_spline(x, z, xnew, order=inp.InterpolOrder,regularization_weight=inp.RegularizationWeight)
    print("shape of y_data_interpolation:::", str(y_data_interpolation.get_shape().as_list()))
    y_data_interpolation=tf.squeeze(y_data_interpolation,axis=2)
    return y_data_interpolation

# create a tf variable from a tuple
def make_variable(tuple, initializer):
  return tf.Variable(initializer(shape=tuple, dtype=tf.float32))

# assign a set of slices in a tf variable
# DOES NOT WORK...
def assign_variable_slice(slices,values,var):
  for i in slices: 
     var=var[i:i+1,:].assign(values)
#    print("assigning slice:"+str(i))
#    print("shape of values:::", str(values.get_shape().as_list()))
#    print("values:::",str(values))
  return var 

inp.Npoints=32
inp.InterpolOrder=4

xesnew=make_variable((1,inp.Npoints),tf.random_uniform_initializer(minval=-1., maxval=1.)) 
xesnew=xesnew[0:1,:].assign(tf.expand_dims(tf.linspace(np.float32(0),np.float32(inp.Npoints-1),inp.Npoints),0))
#print("shape of xesnew:::", str(xesnew.get_shape().as_list()))
#print("xesnew:::",str(xesnew))

xes=make_variable((1,int(inp.Npoints/2)),tf.random_uniform_initializer(minval=-1., maxval=1.)) 
xes=xes[0:1,:].assign(tf.expand_dims(tf.linspace(np.float32(0),np.float32(inp.Npoints-1)/4.0,int(inp.Npoints/2)),0))
#print("shape of xes:::", str(xes.get_shape().as_list()))
#print("xes:::",str(xes))

#xesnew=assign_variable_slice([1],tf.expand_dims(tf.linspace(np.float32(0),np.float32(inp.Npoints-1),inp.Npoints),0),xesnew)
#xes=assign_variable_slice([1],tf.expand_dims(tf.linspace(np.float32(0),np.float32(inp.Npoints-1),int(inp.Npoints/2)),0),xes)

xesnew=tf.squeeze( xesnew, axis=0)
xes=tf.squeeze( xes, axis=0)

ys=K.square(xes)
#ys=tf.expand_dims(ys,0)

ys=tf.stack([xes,ys])
print("shape of ys:::", str(ys.get_shape().as_list()))

ynew=make_variable((1,inp.Npoints),tf.random_uniform_initializer(minval=-1., maxval=1.)) 
ynew=ApplyInterpolation(ys,ynew)
ynew=tf.squeeze(ynew,axis=0)
print("shape of ys after interpolation:::", str(ys.get_shape().as_list()))
xes,ys=tf.unstack(ys,axis=0)
print("shape of ys after unstacking:::", str(ys.get_shape().as_list()))
#ys=tf.squeeze(ys,axis=0)


fig, ax = plt.subplots()
ax.plot( xesnew, ynew,label='interpolation', marker='+')
ax.plot(xes, ys,label='data', marker='o')

ax.set(xlabel='point', ylabel='value')
ax.grid()
ax.legend()

fig.savefig("test_interpolate_spline.png")
plt.show()


