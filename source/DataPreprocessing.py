# This file collects routines to preprocess the single-shot data for the respective machine learning tasks

#########################################################
#########                 IMPORTS               #########
#########################################################

#portability imports:
from __future__ import print_function

#predefined packages:
import numpy as np
import sys
import matplotlib.pyplot as plt # for plotting history etc
##################################################################################################################

#custom-made classes and modules:
import Input as inp  # import input file


##################################################################################################################
def main(x_data,y_data):
    """
    Main routine to perform all the data preprocessing tasks.
    
    Parameters
    ----------
    x_data: numpy array
        independent data for the machine learning task
    
    y_data: numpy array
        labels for the machine learning task
    
    Returns
    -------
    NONE, calls subroutines to exectue runtime checks and manipulate the data.
    

    References
    ----------

    See Also
    --------

    Notes
    -----

    Examples
    --------

    """
    #some options
    np.set_printoptions(threshold=sys.maxsize)
    #some global quantities for this file:
    NSamples=int(inp.NShots/inp.NShotsPerSample) # number of samples, i.e., stacks of single shots
    

    #refine the data baseed on selected sub-intervals etc:
    #for the moment it does nothing!
    x_data_refined, y_data_refined, NDatasetsNew = data_refiner(x_data, y_data)
    
    #manipulate data depending on the task:
    if inp.Job_Type=='SUPERV_REGR':
        x_data_preprocessed, y_data_preprocessed = manipulate_super_regr(x_data_refined, y_data_refined, NDatasetsNew)
        return x_data_preprocessed, y_data_preprocessed
        
    if inp.Job_Type=='SUPERV_CLASS':
        manipulate_super_class()
        return
    if inp.Job_Type=='UNSUPERV_REGR':
        manipulate_unsuper_regr()
        return
    if inp.Job_Type=='UNSUPERV_CLASS':
        manipulate_unsuper_class()
        return

##################################################################################################################


##################################################################################################################
#########            DATA EXTRACTION            #########
#########################################################
def data_refiner(x_data, y_data):
    """
    Refines the data to exclude values not within the provided bounds.
    
    Parameters
    ----------
    NONE
       

    Returns
    -------
    x_data: numpy array, dimension depending on input
        array of data ("x data") on which to perform machine learning
    
    y_data: numpy array, dimension depending on input
        array of labels ("y data") to use as classifiers

    NDatasetsNew: numpy array, dimension depending on input
        full dataset reshaped depending on possible bounds imposed on the value of y
    
    References
    ----------
    
    See Also
    --------

    Notes
    -----

    Examples
    --------
    """
    
    #some options
    np.set_printoptions(threshold=sys.maxsize)
    #some global quantities for this file:
    NSamples=int(inp.NShots/inp.NShotsPerSample) # number of samples, i.e., stacks of single shots

    if inp.Training_Data=='SSS':     #training data: single shot simulations
    
        ########################################################
        ### Extraction of fragmentation/particle number data ###
        ########################################################
                  
        # make sure that the data is numpy arrays (x_data is a 2d array, y_data is a 1d array)
        x_data = np.array(x_data)
        y_data = np.array(y_data)
            
        #TODO: COMPLETE/VERIFY THIS DATA SELECTION AND EXTEND TO DENSITY/CORRELATION.
        # On my installation the np.delete did nothing, so I commented this for the time being.
        NDatasetsNew=np.shape(x_data)[0]

#        if (inp.Learn=='FRAG' or inp.Learn=='NPAR'):
#            #restrict the training data only to a certain interval:
#            #first loop over all values of fragmentation/particle number to determine the indices of the
#            #values that should be removed:
#            indices_to_kill = []
#            if (inp.Learn=='FRAG'):
#                for i in range(len(y_data)):
#                    if (y_data[i] < inp.FragmentationLowerBound or y_data[i] > inp.FragmentationUpperBound):
#                        indices_to_kill.append(i)
#            elif (inp.Learn=='NPAR'):
#                for i in range(len(y_data)):
#                    if (y_data[i] < inp.NPartLowerBound or y_data[i] > inp.NPartUpperBound):
#                        indices_to_kill.append(i)
#                            
#            print("OLD shape of x_data:", np.shape(x_data))
#            print("OLD shape of y_data:", np.shape(y_data))
#            #now remove those indices from both the x_data (NDatasets*NShots x NPoints array) and y_data (NDatasets*Nshots array):
#            x_data = np.delete(x_data,indices_to_kill,axis=0)
#            y_data = np.delete(y_data,indices_to_kill,axis=0)
#
#            #taking into account removed elements for our future reshaping:
#            NRemoved = int(len(indices_to_kill)/inp.NShots)
#            NDatasetsNew=inp.NDatasets-NRemoved
#              
#            #checking the shape of the arrays now:
#            print("Checking the shape of the array with elements removed... ")
#            print("New shape of x_data:", np.shape(x_data))
#            print("New shape of y_data:", np.shape(y_data))
#            print("Number of elements removed:",NRemoved*inp.NShots)
#            print("Number of data sets removed:",NRemoved)
#            print("New Number of data sets:",NDatasetsNew)
#            x_data=np.reshape(x_data,(NSamples*NDatasetsNew,inp.Npoints,inp.NShotsPerSample))
#            print("FINAL shape of x_data:", np.shape(x_data))
#         
#        else:
#            NDatasetsNew=inp.NDatasets


    return x_data, y_data, NDatasetsNew
##################################################################################################################



###########################################################################
#########       DATA MANIPULATION FOR SUPERVISED REGRESSION       #########
###########################################################################
def manipulate_super_regr(x_data, y_data, NDatasetsNew):

    """
    Reshape data differently depending on which quantity is to be learned for SUPERVISED REGRESSION.
        
    Parameters
    ----------
    x_data: numpy array, dimension depending on input
        array of data ("x data") on which to perform machine learning
    
    y_data: numpy array, dimension depending on input
        array of labels ("y data") to use as classifiers

    NDatasetsNew: numpy array, dimension depending on input
        full dataset reshaped depending on possible bounds imposed on the value of y
        
    Returns
    -------
    x_data_preprocessed: numpy array, dimension depending on input
        array of data ("x data") used to train the neural network
    
    y_data_preprocessed: numpy array, dimension depending on input
        array of labels ("y data") to use as classifiers in the training of the neural network
                    
    References
    ----------
    
    See Also
    --------

    Notes
    -----

    Examples
    --------


    """
    #Call input quantities and derive some others:
    Npoints=inp.Npoints
    NShotsPerSample=inp.NShotsPerSample
    NShots=inp.NShots
    batch_size=inp.batch_size
    NSamples=int(batch_size*NShots/NShotsPerSample)
    NSamplesPerDataset=int(NSamples/batch_size)
    
    # So far the xdata per batch has the following shape*:
    # (number of wavefunctions per batch=batch_size,NShots,Npoints)
    # *: this does not consider possible data refinement for interval selection etc.
    # TODO: update including this. NDatasetsNew must always be divisible by number of batches, but can't know a priori.
    
    # Now we will reshape the data into a stack of images (Samples) of shape (inp.Npoints,inp.NShotsPerSample)
    # This is because the CNN models have input_shape=(inp.Npoints,inp.NShotsPerSample,1).
    # e.g.:
    # (5 wavefunctions, 100 single shots per wave function, 256 points per single shot)
    # converted to
    # (10 samples, 256 points per single shot, 50 single shots per sample)
    #
    # 5x100=500 -> 10x50=500
    #
    if inp.ConvNet==True: # for CNNs
        x_data_preprocessed=np.reshape(x_data,(NSamples,NShotsPerSample,Npoints))
    else: # for MLPs
        x_data_preprocessed=np.reshape(x_data,(NSamples,Npoints*NShotsPerSample))


    #TODO: UNIFY ALL CASES SINCE THEY HAVE THE SAME STRUCTURE...
    ######################################################################
    #SCALARS: Fragmentation, Particle number, ...
    ######################################################################
    if inp.Learn=='FRAG' or inp.Learn=='NPAR':
        # Setup array for label data
        y_data_preprocessed=np.empty([NSamples],dtype=np.float32)
        #Add 99 everywhere as a consistency check to see that the later assignment of elements in y_data_int was correct:
        #y_data_int[0:NDatasetsNew*NShots-1]=99.0
        #y_data_int=np.array(y_data_int)
        # Fill label array (all the shots from the
        # same wavefunction correspond to the same label!)
        for i in range(batch_size):
            for j in range(NSamplesPerDataset):
                y_data_preprocessed[NSamplesPerDataset*i + j]=y_data[i]
    ######################################################################
            
    ######################################################################
    #VECTORS: real space density, momentum space density, ...
    ######################################################################
    # Creating the label array for the density:
    elif inp.Learn=='DENS' or inp.Learn=='POT' or inp.Learn=='PHASEHIST':
        y_data_preprocessed=np.zeros((NSamples,Npoints),dtype='float32')
        # Fill label array of training data
        for i in range(batch_size):
            for j in range(NSamplesPerDataset):
                y_data_preprocessed[NSamplesPerDataset*i + j]=y_data[i]
    ######################################################################

    ######################################################################
    #MATRICES: g(1) correlation function, g(2) correlation function, ...
    ######################################################################
    # Creating the label array for the correlations:
    elif inp.Learn=='CORR1' or inp.Learn=='CORR2' or inp.Learn=='RHO1' or inp.Learn=='RHO2':
        y_data_preprocessed=np.zeros((NSamples,Npoints*Npoints),dtype='float32')
        # Fill label array of training data
        for i in range(batch_size):
            for j in range(NSamplesPerDataset):
                y_data_preprocessed[NSamplesPerDataset*i + j]=y_data[i]
    ######################################################################

    # uncomment the following for outputting the classification/labels of the whole training set
    #print('Classification:'+str(y_data_int))

    #TODO: check implementation with CNN for regression of fragmentation, particle number?
    #if kwargs.get('ConvNet',bool)==True: # for CNNs, add one dimension
    if inp.ConvNet==True:
        x_data_preprocessed=np.expand_dims(x_data_preprocessed,axis=3)
        
    x_data_preprocessed = x_data_preprocessed.astype('float32')
    ###########################################################################

    # output the shapes of the data to screen
    #print('SHAPE OF DATA::'+str(x_data_preprocessed.shape))
    #print('SHAPE OF LABELS::'+str(y_data_preprocessed.shape))

    return x_data_preprocessed, y_data_preprocessed
##################################################################################################################

#TODO: all of this
###############################################################################
#########       DATA MANIPULATION FOR SUPERVISED CLASSIFICATION       #########
###############################################################################
def manipulate_super_class():
    """
    Not yet implemented.
    
    """
    print('NOT YET IMPLEMENTED!')
    exit()
    return
###############################################################################

###############################################################################
#########       DATA MANIPULATION FOR UNSUPERVISED REGRESSION         #########
###############################################################################
def manipulate_unsuper_regr():
    """
    Not yet implemented.
    
    """
    print('NOT YET IMPLEMENTED!')
    exit()
    return
###############################################################################

###############################################################################
#########       DATA MANIPULATION FOR UNSUPERVISED CLASSIFICATION       #######
###############################################################################
def manipulate_unsuper_class():
    """
    Not yet implemented.
    
    """
    print('NOT YET IMPLEMENTED!')
    exit()
    return
##################################################################################################################

##################################################################################################################

if __name__ == "__main__":
    main()
